package upc.fib.perez.lluis.cluki.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import org.apache.commons.lang3.ArrayUtils;
import upc.fib.perez.lluis.cluki.R;
import upc.fib.perez.lluis.cluki.entities.ProcessParticipation;

import java.text.SimpleDateFormat;
import java.util.Locale;


public class GridViewProcessAdapter extends BaseAdapter {

    private final LayoutInflater mLayoutInflater;
    private ProcessParticipation processParticipation[] = {};


    public GridViewProcessAdapter(Context c, ProcessParticipation processParticipation[]) {
        this.processParticipation = processParticipation;
        mLayoutInflater = LayoutInflater.from(c);
    }

    public void remove(int position) {
        processParticipation = ArrayUtils.remove(processParticipation, position);
    }

    @Override
    public int getCount() {
        return processParticipation.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yy", Locale.ITALIAN);
        TextHolder holder;
        if (convertView == null) {
            holder = new TextHolder();
            convertView = mLayoutInflater.inflate(R.layout.gridview_item_process, parent, false);
            holder.text = (TextView) convertView.findViewById(R.id.date);
            holder.text2 = (TextView) convertView.findViewById(R.id.percentage);
            holder.text3 = (TextView) convertView.findViewById(R.id.className);
            convertView.setTag(holder);
        } else {
            holder = (TextHolder) convertView.getTag();
        }
        holder.text.setText(simpleDateFormat.format(processParticipation[position].getProcessStartDate()));
        String text1 = processParticipation[position].getParticipationKids() + "/" + processParticipation[position].getCourseKids();
        holder.text2.setText(text1);
        holder.text3.setText(processParticipation[position].getCourseName());
        return convertView;
    }
}
