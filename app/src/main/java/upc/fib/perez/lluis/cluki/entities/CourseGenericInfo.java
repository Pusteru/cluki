package upc.fib.perez.lluis.cluki.entities;

import android.database.Cursor; /**
 * Created by Pusteru on 23/09/2017.
 */
public class CourseGenericInfo {

    private String courseName;
    private int kidNumber;
    private int processesNumber;
    private int courseId;

    public CourseGenericInfo(Cursor c) {
        setCourseId(c.getInt(c.getColumnIndex("course_id")));
        setCourseName(c.getString(c.getColumnIndex("course_name")));
        setKidNumber(c.getInt(c.getColumnIndex("kid_number")));
        setProcessesNumber(c.getInt(c.getColumnIndex("process_number")));
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public int getKidNumber() {
        return kidNumber;
    }

    public void setKidNumber(int kidNumber) {
        this.kidNumber = kidNumber;
    }

    public int getProcessesNumber() {
        return processesNumber;
    }

    public void setProcessesNumber(int processesNumber) {
        this.processesNumber = processesNumber;
    }

    public int getCourseId() {
        return courseId;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }
}
