package upc.fib.perez.lluis.cluki.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.facebook.android.crypto.keychain.AndroidConceal;
import com.facebook.android.crypto.keychain.SharedPrefsBackedKeyChain;
import com.facebook.crypto.Crypto;
import com.facebook.crypto.CryptoConfig;
import com.facebook.crypto.Entity;
import com.facebook.crypto.keychain.KeyChain;

import java.io.*;

/**
 * Created by Pusteru on 26/04/2017.
 */

public class CryptoUtils {

    private final Context context;

    private final Crypto crypto;

    public CryptoUtils(Context context) {
        this.context = context;
        KeyChain keyChain = new SharedPrefsBackedKeyChain(context, CryptoConfig.KEY_256);
        this.crypto = AndroidConceal.get().createDefaultCrypto(keyChain);
    }

    public void encodeAndSaveFile(Bitmap photo, String name) {
        try {
            File file = new File(context.getFilesDir(), name);

            if (!crypto.isAvailable()) {
                return;
            }

            OutputStream fileStream = new BufferedOutputStream(
                    new FileOutputStream(file));

            OutputStream outputStream = crypto.getCipherOutputStream(
                    fileStream, Entity.create(name));
            outputStream.write(bitmapToBytes(photo));
            outputStream.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // convert Bitmap to bytes
    private byte[] bitmapToBytes(Bitmap photo) {

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        photo.compress(Bitmap.CompressFormat.PNG, 100, stream);
        return stream.toByteArray();
    }


    public Bitmap decodeFile(String filename) {

        try {
            File file = new File(context.getFilesDir(), filename);
            if (file.exists()) {
                FileInputStream fileStream = new FileInputStream(file);
                InputStream inputStream = crypto.getCipherInputStream(fileStream,
                        Entity.create(filename));
                Bitmap photo = BitmapFactory.decodeStream(inputStream);

                inputStream.close();
                return photo;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void deleteFile(String filename) {
        File file = new File(context.getFilesDir(), filename);
        if (file.exists()) {
            file.delete();
        }
    }
}
