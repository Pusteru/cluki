package upc.fib.perez.lluis.cluki.activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Switch;
import upc.fib.perez.lluis.cluki.R;

public class Settings extends AppCompatActivityCluki {

    private Switch sw;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        initToolbar("Ajustes", R.id.base_toolbar);
        sw = (Switch) findViewById(R.id.switch2);
        sw.setChecked(imageState());
    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    private boolean imageState() {
        SharedPreferences sharedPref = this.getSharedPreferences("Preferences", Context.MODE_PRIVATE);
        String user = sharedPref.getString("user", "admin");
        return sharedPref.getString(user + "_image", "true").equals("true");
    }

    public void onImageChange(View v) {
        SharedPreferences sharedPref = this.getSharedPreferences("Preferences", Context.MODE_PRIVATE);
        String user = sharedPref.getString("user", "admin");
        String image = sharedPref.getString(user + "_image", "true");
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(user + "_image", image.equals("true") ? "false" : "true");
        editor.apply();
        sw.setChecked(!image.equals("true"));
    }


}
