package upc.fib.perez.lluis.cluki.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import upc.fib.perez.lluis.cluki.dto.Course;
import upc.fib.perez.lluis.cluki.dto.User;

import java.util.ArrayList;
import java.util.List;

public class CourseDAO extends BaseDAO{

    public CourseDAO(Context context){
        super(context);
    }

    private static final String CREATE_COURSES_TABLE = String.format("create table %s (%s integer primary key, %s text, %s text,foreign key (%s) references %s (%s))",
            Course.getTableName(), Course.getColumnId(), Course.getColumnName(), Course.getColumnUser(), Course.getColumnUser(), User.getTableName(), User.getColumnId());

    public static String getCreateCoursesTable() {
        return CREATE_COURSES_TABLE;
    }


    public List<Course> getCourses() {
        List<Course> ret = new ArrayList<>();
        Cursor c = db.rawQuery("select * from courses where user = ?", new String[]{this.getUser()});
        while (c.moveToNext()) {
            Course course = new Course();
            course.setName(c.getString(c.getColumnIndex(Course.getColumnName())));
            course.setId(c.getInt(c.getColumnIndex(Course.getColumnId())));
            ret.add(course);
        }
        c.close();
        return ret;
    }

    public void insertCourse(Course course) {
        ContentValues values = new ContentValues();
        values.put(Course.getColumnId(), course.getId());
        values.put(Course.getColumnName(), course.getName());
        values.put(Course.getColumnUser(), this.getUser());
        db.insert(Course.getTableName(), null, values);
    }

    public Integer getNewCourseId() {
        return getMaxIdFrom(Course.getTableName(), Course.getColumnId());
    }

    public void deleteCourse(int id) {
        db.delete(Course.getTableName(), "id = " + id, null);
    }


}
