package upc.fib.perez.lluis.cluki.entities;

import android.database.Cursor;
import upc.fib.perez.lluis.cluki.dto.Kid;

public class KidInformation {
    private Integer process;
    private Integer group;
    private Integer id;
    private String name;

    public KidInformation(Cursor c) {
        setId(c.getInt(c.getColumnIndex(Kid.getColumnId())));
        setProcess(c.getInt(c.getColumnIndex("process")));
        setName(c.getString(c.getColumnIndex(Kid.getColumnName())));
        double rate = c.getDouble(c.getColumnIndex("rate"));
        setGroup(getGroup(rate));
    }

    public Integer getProcess() {
        return process;
    }

    public void setProcess(Integer process) {
        this.process = process;
    }

    public Integer getGroup() {
        return group;
    }

    public void setGroup(Integer group) {
        this.group = group;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    int getGroup(Double popularIndex) {
        int group = 4;
        if (popularIndex < 0.1) {
            group = 0;
        } else if (popularIndex < 0.15) {
            group = 1;
        } else if (popularIndex < 0.225) {
            group = 2;
        } else if (popularIndex < 0.325) {
            group = 3;
        }
        return group;
    }

}
