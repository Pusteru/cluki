package upc.fib.perez.lluis.cluki.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;
import upc.fib.perez.lluis.cluki.R;
import upc.fib.perez.lluis.cluki.adapters.GridViewKidInformationAdapter;
import upc.fib.perez.lluis.cluki.dao.KidDAO;
import upc.fib.perez.lluis.cluki.dao.KidInformationDAO;
import upc.fib.perez.lluis.cluki.dao.RelationDAO;

/**
 * Created by Pusteru on 02/09/2017.
 */
public class KidInformationSelection extends AppCompatActivityCluki {

    private static final int DEFAULT_INTENT_VALUE = 0;

    private MenuItem edit;
    private MenuItem bin;

    private int position;
    private upc.fib.perez.lluis.cluki.entities.KidInformation[] list;

    private GridViewKidInformationAdapter contentAdapter;
    private Integer course;
    private String name;
    private KidInformationDAO kidInformationDAO;
    private KidDAO kidDAO;
    private RelationDAO relationDAO;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kid_information_selection);

        kidInformationDAO = new KidInformationDAO(this);
        kidDAO = new KidDAO(this);
        relationDAO = new RelationDAO(this);

        initToolbar("Información", R.id.edit_toolbar);
        initContext();
        initGridView();
        floatingButtonListener();
    }


    private void floatingButtonListener() {
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fabClass);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                Intent i = new Intent(mContext, CourseCreation.class);
                i.putExtra("editMode", false);
                i.putExtra("id", course);
                i.putExtra("name", name);
                startActivityForResult(i, 1);
            }
        });
    }

    private void initGridView() {
        list = this.getKidListFromIntent();
        GridView gridView = (GridView) findViewById(R.id.grid_view_class_selection);
        contentAdapter = new GridViewKidInformationAdapter(this, list, crypto);
        gridView.setAdapter(contentAdapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                Intent i = new Intent(mContext, KidInformation.class);
                i.putExtra("id", list[position].getId());
                i.putExtra("name", list[position].getName());
                startActivityForResult(i, 1);
            }
        });
        gridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                           int pos, long arg3) {
                changeMenuVisibility(true);
                position = pos;
                return true;
            }
        });
    }

    private upc.fib.perez.lluis.cluki.entities.KidInformation[] getKidListFromIntent() {
        Intent i = this.getIntent();
        course = i.getIntExtra(getString(R.string.course), DEFAULT_INTENT_VALUE);
        name = i.getStringExtra("name");
        return kidInformationDAO.getKidInformationBy(course);
    }


    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        getMenuInflater().inflate(R.menu.menu_edit, menu);
        edit = menu.findItem(R.id.edit);
        bin = menu.findItem(R.id.erase);
        changeMenuVisibility(false);
        return true;
    }

    private void changeMenuVisibility(boolean visibility) {
        edit.setVisible(visibility);
        bin.setVisible(visibility);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_FINISH) {
            finish();
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.edit:
                Intent i = new Intent(mContext, CourseCreation.class);
                i.putExtra("editMode", true);
                i.putExtra("kidId", list[position].getId());
                i.putExtra("id", course);
                i.putExtra("name", name);
                startActivityForResult(i, 1);
                break;
            case R.id.erase:
                deleteKidFromGridView();
                break;
        }
        changeMenuVisibility(false);
        return true;
    }

    private void deleteKidFromGridView() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("¿Estas seguro que quieres eliminarlo?")
                .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        crypto.deleteFile("kid_" + list[position].getId());
                        kidDAO.deleteKid(list[position].getId());
                        relationDAO.deleteRelationByKid(list[position].getId());
                        contentAdapter.remove(position);
                        contentAdapter.notifyDataSetChanged();
                        Toast.makeText(mContext, "El alumno ha sido borrado", Toast.LENGTH_SHORT).show();
                    }
                })
                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                })
                .show();
    }
}