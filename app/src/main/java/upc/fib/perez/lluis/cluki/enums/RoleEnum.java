package upc.fib.perez.lluis.cluki.enums;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Pusteru on 25/09/2017.
 */
public enum RoleEnum {

    UNREGISTERED(-1),
    REGISTERED(0),
    SUPERUSER(1);

    private static final Map<Integer, RoleEnum> BY_CODE_MAP = new LinkedHashMap<>();

    static {
        for (RoleEnum rae : RoleEnum.values()) {
            BY_CODE_MAP.put(rae.value, rae);
        }
    }

    private int value;

    RoleEnum(int value) {
        this.value = value;
    }

    public static RoleEnum forCode(int code) {
        return BY_CODE_MAP.get(code);
    }

    public int getValue() {
        return value;
    }

}
