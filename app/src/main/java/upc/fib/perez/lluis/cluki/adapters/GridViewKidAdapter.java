package upc.fib.perez.lluis.cluki.adapters;

import android.content.Context;
import upc.fib.perez.lluis.cluki.R;
import upc.fib.perez.lluis.cluki.entities.ParticipationKid;
import upc.fib.perez.lluis.cluki.utils.CryptoUtils;

/**
 * Created by Pusteru on 19/09/2017.
 */
public class GridViewKidAdapter extends GridViewImageTextAdapter {

    public GridViewKidAdapter(Context c, CryptoUtils crypto, ParticipationKid[] info) {
        super(c, crypto, info);
    }

    @Override
    public int getGridImageContainer() {
        return R.layout.gridview_item_kid;
    }
}
