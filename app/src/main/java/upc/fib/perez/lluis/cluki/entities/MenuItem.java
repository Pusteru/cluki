package upc.fib.perez.lluis.cluki.entities;

/**
 * Created by Pusteru on 20/09/2017.
 */
public class MenuItem {

    private String title;
    private int image;
    private Class action;

    public MenuItem(String title, int image, Class action) {
        this.title = title;
        this.image = image;
        this.action = action;
    }

    public Class getAction() {
        return action;
    }

    public void setAction(Class action) {
        this.action = action;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
