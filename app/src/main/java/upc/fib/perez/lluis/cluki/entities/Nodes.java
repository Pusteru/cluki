package upc.fib.perez.lluis.cluki.entities;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Pusteru on 03/09/2017.
 */
public class Nodes implements Iterable<Node> {
    private final List<Node> nodes;

    public Nodes() {
        nodes = new ArrayList<>();
    }

    public void addNodes(Node n) {
        nodes.add(n);
    }

    public List<Node> getNodes() {
        return nodes;
    }


    @Override
    public Iterator<Node> iterator() {
        return nodes.iterator();
    }

    public boolean contains(Node emptyNode) {
        for (Node node : nodes) {
            if (node.getId() == emptyNode.getId()) {
                return true;
            }
        }
        return false;
    }

    public void merge(Nodes emptyNodes) {
        for (Node emptyNode : emptyNodes) {
            if (!contains(emptyNode)) {
                addNodes(emptyNode);
            }
        }
    }
}
