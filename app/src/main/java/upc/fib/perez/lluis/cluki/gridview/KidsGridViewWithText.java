package upc.fib.perez.lluis.cluki.gridview;

import android.widget.BaseAdapter;
import android.widget.GridView;
import upc.fib.perez.lluis.cluki.entities.ParticipationKid;

/**
 * Created by Pusteru on 02/07/2017.
 */
public class KidsGridViewWithText extends KidsGridView {

    public KidsGridViewWithText(GridView gridView, BaseAdapter imageAdapter, ParticipationKid[] list) {
        super(gridView, imageAdapter, list);
    }

    @Override
    public void selectionOfKid() {

    }
}
