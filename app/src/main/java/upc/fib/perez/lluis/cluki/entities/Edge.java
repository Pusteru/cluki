package upc.fib.perez.lluis.cluki.entities;

/**
 * Created by Pusteru on 03/09/2017.
 */
public class Edge {
    private int from;
    private int to;
    private String arrows;

    public Edge(int from, int to) {
        this.from = from;
        this.to = to;
        this.arrows = "to";
    }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public int getTo() {
        return to;
    }

    public void setTo(int to) {
        this.to = to;
    }

    public String getArrows() {
        return arrows;
    }

    public void setArrows(String arrows) {
        this.arrows = arrows;
    }
}
