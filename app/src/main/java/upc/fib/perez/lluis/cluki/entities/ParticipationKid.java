package upc.fib.perez.lluis.cluki.entities;

import upc.fib.perez.lluis.cluki.dto.Kid;

/**
 * Created by Pusteru on 20/08/2017.
 */
public class ParticipationKid {

    private Kid kid;
    private boolean participation;

    public ParticipationKid(Kid kid, boolean participation) {
        this.kid = kid;
        this.participation = participation;

    }

    public Kid getKid() {
        return kid;
    }

    public void setKid(Kid kid) {
        this.kid = kid;
    }

    public boolean isParticipation() {
        return participation;
    }

    public void setParticipation(boolean participation) {
        this.participation = participation;
    }
}
