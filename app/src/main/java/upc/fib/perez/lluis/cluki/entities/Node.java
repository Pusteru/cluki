package upc.fib.perez.lluis.cluki.entities;

/**
 * Created by Pusteru on 03/09/2017.
 */
public class Node {
    private int id;
    private String shape;
    private String label;
    private int group;
    private Color color;

    public Node(int id, String name, int group, int extrovert) {
        this.id = id;
        this.label = name;
        this.shape = "dot";
        this.group = group;
        this.color = new Color();
        this.color.setBorder("#FFFFFF");
        switch (group) {
            case 0:
                this.color.setBackground("#fc3d32");
                break;
            case 1:
                this.color.setBackground("#FEA733");
                break;
            case 2:
                this.color.setBackground("#FFFF33");
                break;
            case 3:
                this.color.setBackground("#CDFA1C");
                break;
            default:
                this.color.setBackground("#90f500");
                break;
        }
        switch (extrovert) {
            case 0:
                this.color.setBorder("#fc3d32");
                break;
            case 1:
                this.color.setBorder("#FEA733");
                break;
            case 2:
                this.color.setBorder("#FFFF33");
                break;
            case 3:
                this.color.setBorder("#CDFA1C");
                break;
            default:
                this.color.setBorder("#90f500");
                break;
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getShape() {
        return shape;
    }

    public void setShape(String shape) {
        this.shape = shape;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getGroup() {
        return group;
    }

    public void setGroup(int group) {
        this.group = group;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

}
