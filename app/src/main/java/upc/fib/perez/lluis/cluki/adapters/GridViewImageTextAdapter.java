package upc.fib.perez.lluis.cluki.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import upc.fib.perez.lluis.cluki.R;
import upc.fib.perez.lluis.cluki.dto.Kid;
import upc.fib.perez.lluis.cluki.entities.ParticipationKid;
import upc.fib.perez.lluis.cluki.utils.CryptoUtils;

import java.io.ByteArrayOutputStream;

/**
 * Created by Pusteru on 01/05/2017.
 */

class GridViewImageTextAdapter extends BaseAdapter {

    private final CryptoUtils crypto;
    private final Context context;
    private ParticipationKid info[] = {};

    GridViewImageTextAdapter(Context c, CryptoUtils crypto, ParticipationKid[] info) {
        this.crypto = crypto;
        this.info = info;
        context = c;
    }

    @Override
    public int getCount() {
        return info.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public Bitmap getImage(int position) {
        Kid selected = this.info[position].getKid();
        String file = "kid_" + selected.getId();
        return this.crypto.decodeFile(file);
    }

    public byte[] bitmapToByte(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        return stream.toByteArray();
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(getGridImageContainer(), parent, false);
            holder.textView = (TextView) convertView.findViewById(R.id.title);
            holder.imageView = (ImageView) convertView.findViewById(R.id.thumbnail);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        ParticipationKid kid = info[position];
        if (kid.isParticipation()) {
            holder.imageView.setBackgroundColor(Color.GREEN);
        }
        setImage(position, holder);
        holder.textView.setText(kid.getKid().getName());
        return convertView;
    }

    public void setImage(int position, ViewHolder holder) {
        Glide.with(holder.imageView.getContext()).asBitmap().load(bitmapToByte(getImage(position))).into(holder.imageView);
    }

    int getGridImageContainer() {
        return R.layout.gridview_item_kid;
    }
}
