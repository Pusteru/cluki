package upc.fib.perez.lluis.cluki.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;
import upc.fib.perez.lluis.cluki.R;
import upc.fib.perez.lluis.cluki.adapters.GridViewProcessAdapter;
import upc.fib.perez.lluis.cluki.dao.CourseDAO;
import upc.fib.perez.lluis.cluki.dao.ProcessDAO;
import upc.fib.perez.lluis.cluki.dao.ProcessParticipationDAO;
import upc.fib.perez.lluis.cluki.dao.RelationDAO;
import upc.fib.perez.lluis.cluki.dto.Course;
import upc.fib.perez.lluis.cluki.entities.ProcessParticipation;

import java.util.ArrayList;
import java.util.List;

public class ProcessSelection extends AppCompatActivityCluki {

    private ListView listView;
    private String[] coursesNames;

    private ProcessDAO processDAO;

    private MenuItem bin;
    private GridViewProcessAdapter adapter;
    private ProcessParticipation[] processesParticipation;
    private int position;
    private CourseDAO courseDAO;
    private ProcessParticipationDAO processParticipationDAO;
    private RelationDAO relationDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_process_selection);

        processDAO = new ProcessDAO(this);
        courseDAO = new CourseDAO(this);
        processParticipationDAO = new ProcessParticipationDAO(this);
        relationDAO = new RelationDAO(this);

        initContext();
        initGridView();
        initToolbar("Selección de Proceso",R.id.base_toolbar);
        floatingButtonListener();
    }

    private void floatingButtonListener(){
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialogListView();
            }
        });
    }

    private void listView() {
        final List<Course> courses = courseDAO.getCourses();
        coursesNames = getCoursesNames(new ArrayList<>(courses));
        ArrayAdapter<String> adapter= new ArrayAdapter<>(this,
                R.layout.listview_item_course_dialog, R.id.dialog_item_class, coursesNames);
        listView=new ListView(this);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new
            AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            int process = processDAO.getNewProcessId(courses.get(position).getId());
            Intent shareIntent = new Intent(mContext, KidProfessorSelection.class);
            shareIntent.putExtra(getString(R.string.process),process);
            shareIntent.putExtra(getString(R.string.course),courses.get(position).getId());
            startActivityForResult(shareIntent,REQUEST_BASIC);
            }
            });
    }

    private String[] getCoursesNames(List<Course> courses) {
        String [] items = new String[courses.size()];
        int count = 0;
        for(Course course: courses){
            items[count++] = course.getName();
        }
        return items;
    }

    private void showDialogListView(){
        AlertDialog.Builder builder=new
                AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle("Escoge una classe");
        listView();
        builder.setView(listView);
        if(coursesNames.length == 0){
            builder.setPositiveButton("Crear Classe",new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    createDialog();
                    dialog.cancel();
                }
            });
            builder.setTitle("No hay ningúna classe para escoger");
        }
        AlertDialog dialog=builder.create();
        dialog.show();

    }

    private void initGridView(){
        processesParticipation = processParticipationDAO.getProcessesParticipation();
        processesParticipation = new ProcessParticipation[0];
        GridView gridView = (GridView) findViewById(R.id.grid_view_preview);
        adapter = new GridViewProcessAdapter(this, processesParticipation);
        gridView.setAdapter(adapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id)   {
                Intent shareIntent = new Intent(v.getContext(), KidProfessorSelection.class);
                shareIntent.putExtra(getString(R.string.process), processesParticipation[position].getProcessId());
                shareIntent.putExtra(getString(R.string.course), processesParticipation[position].getCourseId());
                startActivityForResult(shareIntent,REQUEST_BASIC);
            }
        });
        gridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                           int pos, long arg3) {
                bin.setVisible(true);
                position = pos;
                return true;
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == REQUEST_BASIC) {
            initGridView();
        } else if(resultCode == RESULT_FINISH){
            finish();
        }
    }


    private void createNewCourse(String text) {
        Integer id = courseDAO.getNewCourseId();
        createNewCourse(id,text);
        Intent i = new Intent(mContext,CourseCreation.class);
        i.putExtra("id",id);
        i.putExtra("name",text);
        startActivityForResult(i,5);
        initGridView();
    }

    private void createNewCourse(Integer id, String text) {
        Course course = new Course();
        course.setName(text);
        course.setId(id);
        courseDAO.insertCourse(course);
    }


    private void createDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setView(R.layout.dialog_inputtext)
                .setTitle("¿Que nombre tiene la classe?")
                .setPositiveButton("Crear", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        EditText editText = (EditText) ((AlertDialog) dialog).findViewById(R.id.dialog_edit_text);
                        if(editText != null && !String.valueOf(editText.getText()).isEmpty()) {
                            String text = String.valueOf(editText.getText());
                            createNewCourse(text);

                        } else if(editText != null){
                            editText.setError("Hay que introducir un nombre");
                        }
                    }
                })
                .show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_edit, menu);
        menu.findItem(R.id.edit).setVisible(false);
        bin = menu.findItem(R.id.erase);
        bin.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.erase:
                deleteProcessFromGridView();
                break;
        }
        bin.setVisible(false);
        return true;
    }

    private void deleteProcessFromGridView() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("¿Estas seguro que quieres eliminarlo?")
                .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        deleteAction();
                    }
                })
                .setNegativeButton("NO",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                })
                .show();
    }

    private void deleteAction() {
        int id = processesParticipation[position].getCourseId();
        crypto.deleteFile("process_" + id);
        processDAO.deleteProcessById(id);
        relationDAO.deleteRelationByProcess(id);
        adapter.remove(position);
        adapter.notifyDataSetChanged();
        Toast.makeText(mContext,"El curso ha sido eliminado",Toast.LENGTH_SHORT).show();
    }


}
