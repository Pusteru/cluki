package upc.fib.perez.lluis.cluki.activities;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.Toast;
import upc.fib.perez.lluis.cluki.R;
import upc.fib.perez.lluis.cluki.adapters.GridViewProfessorAdapter;
import upc.fib.perez.lluis.cluki.dao.UserDAO;
import upc.fib.perez.lluis.cluki.dto.User;
import upc.fib.perez.lluis.cluki.enums.RoleEnum;

public class ProfessorCreation extends AppCompatActivityCluki {

    private User[] users;
    private GridViewProfessorAdapter adapter;
    private int position;
    private UserDAO userDAO;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_professor_creation);

        userDAO = new UserDAO(this);

        initContext();
        initToolbar("Profesores", R.id.base_toolbar);
        initGridView();
        floatingButtonListener();
    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }


    private void initGridView() {
        users = userDAO.getUsers();
        GridView gridView = (GridView) findViewById(R.id.grid_view_class_selection);
        adapter = new GridViewProfessorAdapter(this, users);
        gridView.setAdapter(adapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int pos, long id) {
                position = pos;
                changeVisibility(true);
            }
        });
    }

    private void changeVisibility(boolean visibility) {
        findViewById(R.id.updateButton).setVisibility(View.GONE);
        findViewById(R.id.removeButton).setVisibility(View.GONE);
        findViewById(R.id.saveButton).setVisibility(View.VISIBLE);
        if (visibility) {
            EditText editTextPassword = (EditText) findViewById(R.id.editPassword);
            EditText editTextUser = (EditText) findViewById(R.id.editUser);
            editTextPassword.setText(users[position].getPassword());
            editTextUser.setText(users[position].getId());
            findViewById(R.id.updateButton).setVisibility(View.VISIBLE);
            if (users[position].getRole() != RoleEnum.SUPERUSER) {
                findViewById(R.id.removeButton).setVisibility(View.VISIBLE);
            }
            findViewById(R.id.saveButton).setVisibility(View.GONE);
        }
    }


    private void floatingButtonListener() {
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fabClass);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                changeVisibility(false);
            }
        });
    }

    public boolean checkUsers(String userId) {
        boolean ret = false;
        for (User user : users) {
            if (user.getId().equals(userId)) {
                ret = true;
            }
        }
        return ret;
    }


    public void saveUser(View v) {
        EditText editTextPassword = (EditText) findViewById(R.id.editPassword);
        EditText editTextUser = (EditText) findViewById(R.id.editUser);
        if (editTextUser.getText().toString().isEmpty()) {
            editTextUser.setError("Usuario vacío");
        } else if (editTextPassword.getText().length() < 10) {
            editTextPassword.setError("Minimo contranseña de 10 caracteres");
        } else if (checkUsers(editTextUser.getText().toString())) {
            editTextUser.setError("El usuario ya existe");
        } else {
            User user = new User();
            user.setPassword(editTextPassword.getText().toString());
            user.setId(editTextUser.getText().toString());
            user.setRole(RoleEnum.REGISTERED);
            userDAO.insertUser(user);
            adapter.addUser(user);
            adapter.notifyDataSetChanged();
            Toast.makeText(this, "El usuario ha sido añadido", Toast.LENGTH_SHORT).show();
        }
    }

    public void edit(View v) {
        EditText editTextPassword = (EditText) findViewById(R.id.editPassword);
        EditText editTextUser = (EditText) findViewById(R.id.editUser);
        if (editTextUser.getText().toString().isEmpty()) {
            editTextUser.setError("Usuario vacío");
        } else if (editTextPassword.getText().length() < 10) {
            editTextPassword.setError("Minimo contranseña de 10 caracteres");
        } else if (!users[position].getId().equals(editTextUser.getText().toString()) && checkUsers(editTextUser.getText().toString())) {
            editTextUser.setError("El usuario ya existe");
        } else {
            User user = new User();
            user.setPassword(editTextPassword.getText().toString());
            user.setId(editTextUser.getText().toString());
            user.setRole(RoleEnum.REGISTERED);
            userDAO.updateUser(users[position].getId(), user);
            adapter.modifyUser(position, user);
            adapter.notifyDataSetChanged();
            changeVisibility(false);
            Toast.makeText(this, "El usuario ha sido modificado", Toast.LENGTH_SHORT).show();
        }
    }

    public void removeUser(View v) {
        userDAO.removeUser(users[position].getId());
        adapter.removeUser(position);
        adapter.notifyDataSetChanged();
        changeVisibility(false);
        Toast.makeText(this, "El usuario ha sido borrado", Toast.LENGTH_SHORT).show();
    }


}
