package upc.fib.perez.lluis.cluki.dto;

import java.util.Date;

/**
 * Created by Pusteru on 22/04/2017.
 */

public class Process {

    private Integer id;
    private Date date;
    private Integer courseId;

    private static final String tableName = "process";
    private static final String columnId = "id";
    private static final String columnDate = "date";
    private static final String columnCourseId = "course_id";
    private static final String columnUser = "user";

    public static String getTableName() {
        return tableName;
    }

    public static String getColumnId() {
        return columnId;
    }

    public static String getColumnDate() {
        return columnDate;
    }

    public static String getColumnCourseId() {
        return columnCourseId;
    }

    public static String getColumnUser() {
        return columnUser;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getCourseId() {
        return courseId;
    }

    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }


}
