package upc.fib.perez.lluis.cluki.dao;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import upc.fib.perez.lluis.cluki.db.DBHelper;
import upc.fib.perez.lluis.cluki.session.UserSessionManager;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

class BaseDAO {

    private String user;
    private final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.GERMAN);
    SQLiteDatabase db;

    BaseDAO(Context context){
        this.user = new UserSessionManager(context).getUserName();
        this.db = new DBHelper(context).getWritableDatabase();
    }

    public String getUser(){
        return user;
    }

    int getMaxIdFrom(String table, String id) {
        Integer ret = 0;
        Cursor c = db.rawQuery(String.format("SELECT MAX(%s) as %s FROM %s", id, id, table), new String[]{});
        if (c.moveToNext()) {
            ret = c.getInt(c.getColumnIndex(id)) + 1;
        }
        c.close();
        return ret;
    }

    String formatDate(Date date){
        return formatter.format(date);
    }

    Date parseDate(String date){
        try {
            return formatter.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

}
