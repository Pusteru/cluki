package upc.fib.perez.lluis.cluki.activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.google.gson.Gson;
import upc.fib.perez.lluis.cluki.R;
import upc.fib.perez.lluis.cluki.dao.KidNumbersDAO;
import upc.fib.perez.lluis.cluki.dao.NodesDAO;
import upc.fib.perez.lluis.cluki.dao.RelationDAO;
import upc.fib.perez.lluis.cluki.interfaces.JavaScriptInterface;

public class GraphView extends AppCompatActivityCluki {

    private static final int DEFAULT_INTENT_VALUE = 0;
    private NodesDAO nodesDAO;
    private KidNumbersDAO kidNumbersDAO;
    private RelationDAO relationDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graphview);
        nodesDAO = new NodesDAO(this);
        kidNumbersDAO = new KidNumbersDAO(this);
        relationDAO = new RelationDAO(this);
        initContext();
        initToolbar("Resultado", R.id.base_toolbar);
        int process = this.getIntent().getIntExtra(getString(R.string.process), DEFAULT_INTENT_VALUE);
        prepareJavascriptWebDataTransmision(process);
    }


    private void prepareJavascriptWebDataTransmision(int process) {
        WebView wv = (WebView) findViewById(R.id.wv);
        wv.addJavascriptInterface(getJavaScriptInterface(process), "Android");
        wv.setWebViewClient(new WebViewClient());
        wv.getSettings().setJavaScriptEnabled(true);
        wv.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        wv.getSettings().setAllowFileAccessFromFileURLs(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            WebView.setWebContentsDebuggingEnabled(true);
        }
        String path = "file:///android_asset/html/graph.html";
        wv.loadUrl(path);
    }


    @NonNull
    private JavaScriptInterface getJavaScriptInterface(int process) {
        Gson gson = new Gson();
        String jsonNodes = gson.toJson(nodesDAO.getNodesFrom(process).getNodes());
        String jsonEdges = gson.toJson(relationDAO.getEdges(process).getEdges());
        String extrovertRate = gson.toJson(kidNumbersDAO.getKidsExtrovert(process));
        String friendlyRate = gson.toJson(kidNumbersDAO.getKidsFriends(process));
        return new JavaScriptInterface(this, jsonNodes, jsonEdges, extrovertRate, friendlyRate, process);
    }

    @Override
    public void onBackPressed() {
        Intent returnIntent = new Intent();
        setResult(RESULT_FINISH, returnIntent);
        finish();
    }
}
