package upc.fib.perez.lluis.cluki.entities;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pusteru on 03/09/2017.
 */
public class Edges {
    private final List<Edge> edges;

    public Edges() {
        this.edges = new ArrayList<>();
    }

    public void addEdge(Edge e) {
        this.edges.add(e);
    }

    public List<Edge> getEdges() {
        return edges;
    }

}
