package upc.fib.perez.lluis.cluki.dto;

import android.database.Cursor;
import upc.fib.perez.lluis.cluki.enums.RoleEnum;

/**
 * Created by Pusteru on 25/09/2017.
 */
public class User {

    private String id;
    private String password;
    private RoleEnum role;

    private static final String tableName = "users";
    private static final String columnId = "id";
    private static final String columnPassword = "password";
    private static final String columnRole = "role";

    public User(Cursor c) {
        setId((c.getString(c.getColumnIndex(User.getColumnId()))));
        setPassword(c.getString(c.getColumnIndex(User.getColumnPassword())));
        setRole(RoleEnum.forCode(c.getInt(c.getColumnIndex(User.getColumnRole()))));
    }

    public static String getTableName() {
        return tableName;
    }

    public static String getColumnId() {
        return columnId;
    }

    public static String getColumnPassword() {
        return columnPassword;
    }

    public static String getColumnRole() {
        return columnRole;
    }

    public User() {

    }

    public User(String mEmail, String mPassword, int i) {
        id = mEmail;
        password = mPassword;
        role = RoleEnum.forCode(i);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public RoleEnum getRole() {
        return role;
    }

    public void setRole(RoleEnum role) {
        this.role = role;
    }

}
