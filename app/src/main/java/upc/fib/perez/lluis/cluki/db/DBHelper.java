package upc.fib.perez.lluis.cluki.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import upc.fib.perez.lluis.cluki.dao.*;
import upc.fib.perez.lluis.cluki.dto.User;

/**
 * Created by Pusteru on 22/04/2017.
 */

public class DBHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "Data.db";
    private static final Integer DATABASE_VERSION = 1;
    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(UserDAO.getCreateUsersTable());
        insertAdminDefaultUser(db);
        db.execSQL(CourseDAO.getCreateCoursesTable());
        db.execSQL(KidDAO.getCreateKidsTable());
        db.execSQL(ProcessDAO.getCreateProcessTable());
        db.execSQL(RelationDAO.getCreateRelationsTable());
    }

    private void insertAdminDefaultUser(SQLiteDatabase db) {
        User user = new User("a", "a", 1);
        ContentValues values = new ContentValues();
        values.put(User.getColumnId(), user.getId());
        values.put(User.getColumnPassword(), user.getPassword());
        values.put(User.getColumnRole(), user.getRole().getValue());
        db.insert(User.getTableName(), null, values);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void closeDB() {
        SQLiteDatabase db = this.getReadableDatabase();
        if (db != null && db.isOpen())
            db.close();
    }



}
