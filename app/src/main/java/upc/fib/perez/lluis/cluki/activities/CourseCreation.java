package upc.fib.perez.lluis.cluki.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.*;
import upc.fib.perez.lluis.cluki.R;
import upc.fib.perez.lluis.cluki.dao.KidDAO;
import upc.fib.perez.lluis.cluki.dto.Kid;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class CourseCreation extends AppCompatActivityCluki {

    private static final int REQUEST_IMAGE_CAPTURE = 2;
    private static final int RESULT_LOAD_IMG = 3;
    private static final int RESULT_CODE = 1;
    private int courseId;
    private EditText editText;
    private ImageView imageView;
    private TextView textView;
    private boolean editMode;
    private Kid editKid;
    private Kid[] kids;
    private KidDAO kidDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course_creation);
        Intent i = this.getIntent();
        initContext();
        courseId = i.getIntExtra("id", 0);
        initComponents();
        initEditText();
        initListView();
        initEditMode();
        initToolbar(i.getStringExtra("name"), R.id.base_toolbar);
        floatingButtonListener();
    }

    private void initEditMode() {
        Intent i = this.getIntent();
        if (i.getBooleanExtra("editMode", false)) {
            int id = i.getIntExtra("kidId", 0);
            Kid ret = new Kid();
            for (Kid kid : kids) {
                if (id == kid.getId()) {
                    ret = kid;
                }
            }
            editMode = true;
            editKid = ret;
            editText.setText(ret.getName());
            textView.setText(ret.getName());
            imageView = (ImageView) findViewById(R.id.thumbnail);
            imageView.setImageBitmap(crypto.decodeFile("kid_" + ret.getId()));
        }
    }

    private void initComponents() {
        editText = (EditText) findViewById(R.id.editText);
        imageView = (ImageView) findViewById(R.id.thumbnail);
        textView = (TextView) findViewById(R.id.title);
        editMode = false;
    }

    private void resetComponents() {
        editText.setText("");
        textView.setText("Prueba");
        imageView.setImageResource(0);
    }

    private void initEditText() {
        editText = (EditText) findViewById(R.id.editText);
        editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (editText != null) {
                    final String name = editText.getText().toString();
                    if (!hasFocus && name.isEmpty()) {
                        textView.setText(name);
                    }
                }
            }
        });
    }

    private void initListView() {
        kids = getKidDAO().getKidsByCourse(courseId);
        List<String> kidNames = kidNames(kids);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                R.layout.listview_item_course_dialog, R.id.dialog_item_class, kidNames);
        ListView listView = (ListView) findViewById(R.id.kid_list);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                changeKidView(kids[position]);
            }
        });
    }

    private void changeKidView(Kid kid) {
        editMode = true;
        editKid = kid;
        editText.setText(kid.getName());
        textView.setText(kid.getName());
        Bitmap b = crypto.decodeFile("kid_" + kid.getId());
        imageView = (ImageView) findViewById(R.id.thumbnail);
        Matrix m = new Matrix();
        m.setRectToRect(new RectF(0, 0, b.getWidth(), b.getHeight()), new RectF(0, 0, imageView.getWidth(), imageView.getHeight()), Matrix.ScaleToFit.CENTER);
        Bitmap escalatekidDAOitmap = Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), m, true);
        imageView.setImageBitmap(escalatekidDAOitmap);
    }


    private void floatingButtonListener() {
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                next();
            }
        });
    }

    private void next() {
        final String name = ((EditText) findViewById(R.id.editText)).getText().toString();
        if (name.isEmpty()) {
            AlertDialog.Builder builder = new
                    AlertDialog.Builder(this);
            builder.setCancelable(true);
            builder.setTitle("No se ha podido guardad");
            builder.setMessage("El niño no tiene nombre");
            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        } else {
            saveKid(name);
            initListView();
            Toast.makeText(this, "Se ha guardado correctamente", Toast.LENGTH_LONG).show();
            resetComponents();
        }
    }


    private void saveKid(String name) {
        Kid kid = new Kid();
        kid.setCourseId(courseId);
        int id = editMode ? editKid.getId() : getKidDAO().getNewKidId();
        kid.setId(id);
        kid.setName(name);
        if (editMode) {
            getKidDAO().updateKid(kid);
        } else {
            getKidDAO().insertKid(kid);
        }
        saveImage(id);

    }

    private void saveImage(int id) {
        imageView = (ImageView) findViewById(R.id.thumbnail);
        imageView.setDrawingCacheEnabled(true);
        Bitmap bmap = imageView.getDrawingCache();
        crypto.encodeAndSaveFile(bmap, "kid_" + id);
        imageView.setDrawingCacheEnabled(false);
    }

    public void takeKidPicture(View v) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    public void getKidPicture(View v) {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, RESULT_LOAD_IMG);
    }


    private List<String> kidNames(Kid[] kids) {
        List<String> names = new ArrayList<>();
        for (Kid kid : kids) {
            names.add(kid.getName());
        }
        return names;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            getImage(requestCode, data);
        } else if (resultCode == RESULT_FINISH) {
            finish();
        }
    }

    private void getImage(int requestCode, Intent data) {
        Bitmap b = getBitmap(requestCode, data);
        Matrix m = new Matrix();
        m.setRectToRect(new RectF(0, 0, b.getWidth(), b.getHeight()), new RectF(0, 0, imageView.getWidth(), imageView.getHeight()), Matrix.ScaleToFit.CENTER);
        Bitmap escalatekidDAOitmap = Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), m, true);
        imageView.setImageBitmap(escalatekidDAOitmap);
    }

    @Nullable
    private Bitmap getBitmap(int requestCode, Intent data) {
        Bitmap bitmapImage = null;
        if (requestCode == REQUEST_IMAGE_CAPTURE) {
            bitmapImage = getImageCaptureBitmap(data);
        } else if (requestCode == RESULT_LOAD_IMG) {
            bitmapImage = getImageLoadBitmap(data);
        }
        return bitmapImage;
    }

    private Bitmap getImageLoadBitmap(Intent data) {
        Bitmap ret = null;
        final Uri imageUri = data.getData();
        try {
            InputStream imageStream = getContentResolver().openInputStream(imageUri);
            ret = BitmapFactory.decodeStream(imageStream);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG).show();

        }
        return ret;
    }

    @Override
    public void onBackPressed() {
        final String name = ((EditText) findViewById(R.id.editText)).getText().toString();
        if (!name.isEmpty()) {
            AlertDialog.Builder builder = new
                    AlertDialog.Builder(this);
            builder.setCancelable(true);
            builder.setTitle("Quieres guardar antes de cerrar");
            builder.setPositiveButton("Si", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    saveKid(name);
                    finish();
                }
            });
            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        } else {
            finish();
        }
    }

    private Bitmap getImageCaptureBitmap(Intent data) {
        Bitmap bitmapImage;
        Bundle extras = data.getExtras();
        bitmapImage = (Bitmap) extras.get("data");
        return bitmapImage;
    }

    private KidDAO getKidDAO(){
        if(kidDAO == null){
            return new KidDAO(this);
        }
        return kidDAO;
    }


}
