package upc.fib.perez.lluis.cluki.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import upc.fib.perez.lluis.cluki.dto.Course;
import upc.fib.perez.lluis.cluki.dto.Process;
import upc.fib.perez.lluis.cluki.dto.Relation;
import upc.fib.perez.lluis.cluki.dto.User;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class ProcessDAO extends BaseDAO{
    
    public ProcessDAO(Context context){
        super(context);
    }

    private static final String createProcessTable = String.format("create table %s (%s integer primary key, %s date, " +
                    "%s integer,%s text,foreign key (%s) references %s (%s), foreign key (%s) references %s (%s)) ",
            Process.getTableName(), Process.getColumnId(), Process.getColumnDate(), Process.getColumnCourseId(), Process.getColumnUser()
            , Process.getColumnUser(), User.getTableName(), User.getColumnId(), Process.getColumnCourseId(), Course.getTableName(), Course.getColumnId());

    public static String getCreateProcessTable() {
        return createProcessTable;
    }

    public void deleteProcessById(int id) {
        db.delete(Process.getTableName(), "id = " + id, null);
    }

    public void deleteProcessByCourse(int id) {
        db.delete(Process.getTableName(), "course_id = " + id, null);
    }

    public int getNewProcessId(int courseId) {
        int id = getMaxIdFrom(Process.getTableName(), Process.getColumnId());
        Process process = new Process();
        process.setId(id);
        process.setDate(new Date());
        process.setCourseId(courseId);
        insertProcess(process);
        return id;
    }

    private void insertProcess(Process process) {
        ContentValues values = new ContentValues();
        values.put(Process.getColumnId(), process.getId());
        values.put(Process.getColumnDate(), this.formatDate(process.getDate()));
        values.put(Process.getColumnCourseId(), process.getCourseId());
        values.put(Process.getColumnUser(), this.getUser());

        db.insert(Process.getTableName(), null, values);
    }

    public List<Process> getProcessFor(Integer kidId) {
        List<Process> ret = new ArrayList<>();
        Cursor c = db.rawQuery("select distinct process_id, date from relations r inner join process p on p.id = r.process_id" +
                " where subject_id = ? and r.user = ?", new String[]{kidId.toString(), this.getUser()});
        while (c.moveToNext()) {
            Process process = new Process();
            process.setId(c.getInt(c.getColumnIndex(Relation.getColumnProcessId())));
            process.setDate(this.parseDate(c.getString(c.getColumnIndex(Process.getColumnDate()))));
            ret.add(process);
        }
        c.close();
        return ret;
    }

    public Integer[] getProcessesIdByCourse(int id) {
        List<Integer> ret = new ArrayList<>();
        String selectionArgs[] = new String[]{String.valueOf(id), this.getUser()};
        Cursor c = db.rawQuery(String.format("select id from %s where course_id = ? and user = ?", Process.getTableName()), selectionArgs);
        while (c.moveToNext()) {
            ret.add(c.getInt(c.getColumnIndex(Process.getColumnId())));
        }
        c.close();
        Integer[] processes = new Integer[ret.size()];
        return ret.toArray(processes);
    }
    
}
