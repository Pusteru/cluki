package upc.fib.perez.lluis.cluki.entities;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pusteru on 07/09/2017.
 */
public class KidNumbers {
    private List<KidNumber> data;

    public KidNumbers() {
        this.data = new ArrayList<>();
    }

    public void addKid(KidNumber kid) {
        data.add(kid);
    }

    public List<KidNumber> getData() {
        return data;
    }

    public void setData(List<KidNumber> data) {
        this.data = data;
    }
}
