package upc.fib.perez.lluis.cluki.adapters;

import android.content.Context;
import android.content.SharedPreferences;
import com.bumptech.glide.Glide;
import upc.fib.perez.lluis.cluki.R;
import upc.fib.perez.lluis.cluki.entities.ParticipationKid;
import upc.fib.perez.lluis.cluki.utils.CryptoUtils;

/**
 * Created by Pusteru on 19/09/2017.
 */
public class GridViewProfessorKidAdapter extends GridViewImageTextAdapter {

    private boolean imageView;

    public GridViewProfessorKidAdapter(Context c, CryptoUtils crypto, ParticipationKid[] info) {
        super(c, crypto, info);
        SharedPreferences sharedPref = c.getSharedPreferences("Preferences", Context.MODE_PRIVATE);
        String user = sharedPref.getString("user", "admin");
        imageView = sharedPref.getString(user + "_image", "true").equals("true");
    }

    @Override
    public int getGridImageContainer() {
        return R.layout.gridview_item_professor_kid;
    }

    @Override
    public void setImage(int position, ViewHolder holder) {
        if (imageView) {
            Glide.with(holder.imageView.getContext()).asBitmap().load(bitmapToByte(getImage(position))).into(holder.imageView);
        }
    }
}
