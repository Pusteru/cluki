package upc.fib.perez.lluis.cluki.dto;

/**
 * Created by Pusteru on 22/04/2017.
 */

public class Course {

    private Integer id;
    private String name;

    private static final String tableName = "courses";
    private static final String columnId = "id";
    private static final String columnName = "name";
    private static final String columnUser = "user";

    public static String getTableName() {
        return tableName;
    }

    public static String getColumnId() {
        return columnId;
    }

    public static String getColumnName() {
        return columnName;
    }

    public static String getColumnUser() {
        return columnUser;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


}
