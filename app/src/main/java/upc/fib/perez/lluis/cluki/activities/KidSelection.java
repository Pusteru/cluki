package upc.fib.perez.lluis.cluki.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.Toast;
import org.apache.commons.lang3.ArrayUtils;
import upc.fib.perez.lluis.cluki.R;
import upc.fib.perez.lluis.cluki.adapters.GridViewKidAdapter;
import upc.fib.perez.lluis.cluki.dao.KidDAO;
import upc.fib.perez.lluis.cluki.dao.RelationDAO;
import upc.fib.perez.lluis.cluki.dto.Kid;
import upc.fib.perez.lluis.cluki.entities.ParticipationKid;
import upc.fib.perez.lluis.cluki.gridview.KidsGridView;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class KidSelection extends AppCompatActivityCluki {

    private static final int DEFAULT_INTENT_VALUE = 0;

    private Integer kidId;
    private KidsGridView kidsGridView;
    private String name;
    private Integer process;
    private KidDAO kidDAO;
    private RelationDAO relationDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kid_selection);
        kidDAO = new KidDAO(this);
        relationDAO = new RelationDAO(this);
        initContext();
        initGridView();
        initToolbar(name, R.id.tool_bar_kid_selection);
    }

    private void initGridView() {
        ParticipationKid[] list = getKidListFromIntent();
        BaseAdapter baseAdapter = new GridViewKidAdapter(this, crypto, list);
        kidsGridView = new KidsGridView((GridView) findViewById(R.id.grid_view_selection), baseAdapter, list);
        kidsGridView.selectionOfKid();
    }

    private ParticipationKid[] getKidListFromIntent() {
        Intent i = this.getIntent();
        Integer course = i.getIntExtra(getString(R.string.course), DEFAULT_INTENT_VALUE);
        Integer position = i.getIntExtra(getString(R.string.position), DEFAULT_INTENT_VALUE);
        process = i.getIntExtra(getString(R.string.process), DEFAULT_INTENT_VALUE);
        Integer kidId = i.getIntExtra(getString(R.string.id), DEFAULT_INTENT_VALUE);
        Kid[] list = kidDAO.getKidsByCourse(course);
        Set<Integer> participation = relationDAO.getKidFriendsIn(process, kidId);
        list = removeTargetKidFromView(list, position);
        return getKidListWithoutSelected(list, participation);
    }

    private Kid[] removeTargetKidFromView(Kid[] list, Integer position) {
        kidId = list[position].getId();
        name = list[position].getName();
        list = ArrayUtils.remove(list, position);
        return list;
    }

    private ParticipationKid[] getKidListWithoutSelected(Kid[] list, Set<Integer> participation) {
        List<ParticipationKid> participationKids = new ArrayList<>();
        for (Kid kid : list) {
            participationKids.add(new ParticipationKid(kid, participation.contains(kid.getId())));
        }
        ParticipationKid[] ret = new ParticipationKid[participationKids.size()];
        return participationKids.toArray(ret);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_arrow, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Integer process = this.getIntent().getIntExtra(getString(R.string.process), DEFAULT_INTENT_VALUE);

        if (item.getItemId() == R.id.action_search) {
            relationDAO.cleanRelationsOf(process,kidId);
            relationDAO.insertRelations(process, kidId, kidsGridView.getSelectedKids());
            Intent returnIntent = new Intent();
            setResult(KidProfessorSelection.RESULT_OK, returnIntent);
            this.finish();
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("¿Quieres guardar los cambios?")
                .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        relationDAO.insertRelations(process, kidId, kidsGridView.getSelectedKids());
                        Intent returnIntent = new Intent();
                        setResult(kidsGridView.getSelectedKids().size() == 0 ? KidProfessorSelection.RESULT_CANCELED
                                : KidProfessorSelection.RESULT_OK, returnIntent);
                        Toast.makeText(mContext, "El niño ha sido guardado", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent returnIntent = new Intent();
                setResult(KidProfessorSelection.RESULT_CANCELED, returnIntent);
                finish();
            }
        })
                .show();

    }

}


