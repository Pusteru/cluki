package upc.fib.perez.lluis.cluki.dto;

public class Relation {

    private static final String tableName = "relations";
    private static final String columnProcessId = "process_id";
    private static final String columnSubjectId = "subject_id";
    private static final String columnFriendId = "friend_id";
    private static final String columnUser = "user";

    public static String getTableName() {
        return tableName;
    }

    public static String getColumnProcessId() {
        return columnProcessId;
    }

    public static String getColumnSubjectId() {
        return columnSubjectId;
    }

    public static String getColumnFriendId() {
        return columnFriendId;
    }

    public static String getColumnUser() {
        return columnUser;
    }

}
