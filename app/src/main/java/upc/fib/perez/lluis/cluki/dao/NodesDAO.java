package upc.fib.perez.lluis.cluki.dao;

import android.content.Context;
import android.database.Cursor;
import upc.fib.perez.lluis.cluki.dto.Kid;
import upc.fib.perez.lluis.cluki.entities.Node;
import upc.fib.perez.lluis.cluki.entities.Nodes;

public class NodesDAO extends BaseDAO{


    public NodesDAO(Context context) {
        super(context);
    }

    public Nodes getNodesFrom(int process) {
        Nodes nodes = new Nodes();
        String query = "select k.id,k.name,k.last_name,count(*) as extrovert, popular from relations r inner join kids " +
                "k on r.subject_id = k.id and r.user = k.user left join (select id,name,last_name,count(*) as popular from relations r " +
                "inner join kids k on r.friend_id = k.id and r.user = k.user where k.user = ? and process_id = ? group by id,name) o on k.id = o.id where process_id = ? and k.user = ? " +
                "group by k.id,k.name;";
        Cursor c = db.rawQuery(query, new String[]{this.getUser(), String.valueOf(process), String.valueOf(process), this.getUser()});
        while (c.moveToNext()) {
            String name = c.getString(c.getColumnIndex(Kid.getColumnName()));
            int id = c.getInt(c.getColumnIndex(Kid.getColumnId()));
            int group = getGroup((double) (c.getInt(c.getColumnIndex("popular"))) / (double) c.getCount());
            int extrovert = getGroup((double) (c.getInt(c.getColumnIndex("extrovert"))) / (double) c.getCount());
            nodes.addNodes(new Node(id, name, group, extrovert));
        }
        c.close();

        Nodes emptyNodes = getEmptyNodesFrom(process);
        nodes.merge(emptyNodes);
        return nodes;
    }

    private Nodes getEmptyNodesFrom(int process) {
        Nodes nodes = new Nodes();
        String query = "select k.id,k.name from kids k inner join relations r on k.id = r.subject_id and " +
                "r.user = k.user where k.user = ? and process_id = ? group by k.id having count(friend_id) = 1 " +
                "and friend_id = -1;";
        Cursor c = db.rawQuery(query, new String[]{this.getUser(), String.valueOf(process)});
        while (c.moveToNext()) {
            String name = c.getString(c.getColumnIndex(Kid.getColumnName()));
            int id = c.getInt(c.getColumnIndex(Kid.getColumnId()));
            nodes.addNodes(new Node(id, name, 0, 0));
        }
        c.close();
        return nodes;
    }

    int getGroup(Double popularIndex) {
        int group = 4;
        if (popularIndex < 0.1) {
            group = 0;
        } else if (popularIndex < 0.15) {
            group = 1;
        } else if (popularIndex < 0.225) {
            group = 2;
        } else if (popularIndex < 0.325) {
            group = 3;
        }
        return group;
    }



}
