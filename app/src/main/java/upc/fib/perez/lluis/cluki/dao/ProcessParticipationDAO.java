package upc.fib.perez.lluis.cluki.dao;

import android.content.Context;
import android.database.Cursor;
import upc.fib.perez.lluis.cluki.entities.ProcessParticipation;

import java.util.ArrayList;
import java.util.List;

public class ProcessParticipationDAO extends BaseDAO{

    public ProcessParticipationDAO(Context context) {
        super(context);
    }

    public ProcessParticipation[] getProcessesParticipation() {
        List<ProcessParticipation> ret = new ArrayList<>();
        Cursor c = db.rawQuery("select p.id,count(distinct k.id) as total,c.num as participants, p.date, co.id as course, co.name " +
                "from process p  " +
                "left join kids k on p.course_id = k.course_id and p.user = k.user  " +
                "inner join  " +
                " (select p.id, count(distinct subject_id) as num  " +
                "  from process p  " +
                "  left join relations r on p.id = r.process_id and p.user = r.user " +
                "  where p.user = ? group by p.id) " +
                "  c on c.id = p.id  " +
                "  left join courses co on co.id = p.course_id and co.user = p.user  " +
                "  where p.user = ? group by p.id", new String[]{this.getUser(), this.getUser()});
        while (c.moveToNext()) {
            ProcessParticipation info = new ProcessParticipation();
            info.setProcessId(c.getInt(c.getColumnIndex("id")));
            info.setCourseKids(c.getInt(c.getColumnIndex("total")));
            info.setParticipationKids(c.getInt(c.getColumnIndex("participants")));
            info.setProcessStartDate(this.parseDate(c.getString(c.getColumnIndex("date"))));
            info.setCourseId(c.getInt(c.getColumnIndex("course")));
            info.setCourseName(c.getString(c.getColumnIndex("name")));
            ret.add(info);
        }
        ProcessParticipation[] processParticipations = new ProcessParticipation[ret.size()];
        c.close();
        return ret.toArray(processParticipations);
    }

}
