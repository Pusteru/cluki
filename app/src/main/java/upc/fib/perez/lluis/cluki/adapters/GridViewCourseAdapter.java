package upc.fib.perez.lluis.cluki.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import org.apache.commons.lang3.ArrayUtils;
import upc.fib.perez.lluis.cluki.R;
import upc.fib.perez.lluis.cluki.entities.CourseGenericInfo;

public class GridViewCourseAdapter extends BaseAdapter {

    private final Context context;
    private CourseGenericInfo[] coursesGenericInfo;

    public GridViewCourseAdapter(Activity context, CourseGenericInfo[] courseGenericInfoList) {
        coursesGenericInfo = courseGenericInfoList;
        this.context = context;
    }

    public void remove(int position) {
        coursesGenericInfo = ArrayUtils.remove(coursesGenericInfo, position);
    }

    @Override
    public int getCount() {
        return coursesGenericInfo.length;
    }

    @Override
    public Object getItem(int position) {
        return coursesGenericInfo[position];
    }

    @Override
    public long getItemId(int position) {
        return coursesGenericInfo[position].getCourseId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextHolder holder;
        if (convertView == null) {
            holder = new TextHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.gridview_item_course, parent, false);
            holder.text = (TextView) convertView.findViewById(R.id.courseName);
            holder.text2 = (TextView) convertView.findViewById(R.id.kidNumber);
            holder.text3 = (TextView) convertView.findViewById(R.id.processNumber);
            convertView.setTag(holder);
        } else {
            holder = (TextHolder) convertView.getTag();
        }
        holder.text.setText(coursesGenericInfo[position].getCourseName());
        holder.text2.setText(String.valueOf(coursesGenericInfo[position].getKidNumber()));
        holder.text3.setText(String.valueOf(coursesGenericInfo[position].getProcessesNumber()));
        return convertView;
    }
}
