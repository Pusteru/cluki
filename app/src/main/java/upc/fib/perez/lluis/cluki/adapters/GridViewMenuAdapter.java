package upc.fib.perez.lluis.cluki.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import upc.fib.perez.lluis.cluki.R;
import upc.fib.perez.lluis.cluki.entities.MenuItem;

/**
 * Created by Pusteru on 20/09/2017.
 */
public class GridViewMenuAdapter extends BaseAdapter {

    private final MenuItem[] items;
    private final Context context;

    public GridViewMenuAdapter(Context context, MenuItem[] items) {
        this.context = context;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.length;
    }

    @Override
    public Object getItem(int position) {
        return items[position];
    }

    @Override
    public long getItemId(int position) {
        return items[position].getImage();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.gridview_item_menu, parent, false);
            holder.textView = (TextView) convertView.findViewById(R.id.title);
            holder.imageView = (ImageView) convertView.findViewById(R.id.thumbnail);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.imageView.setImageResource(items[position].getImage());
        holder.textView.setText(items[position].getTitle());
        return convertView;
    }
}
