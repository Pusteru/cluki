package upc.fib.perez.lluis.cluki.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.Toast;
import upc.fib.perez.lluis.cluki.R;
import upc.fib.perez.lluis.cluki.adapters.GridViewCourseAdapter;
import upc.fib.perez.lluis.cluki.dao.*;
import upc.fib.perez.lluis.cluki.dto.Course;
import upc.fib.perez.lluis.cluki.dto.Kid;
import upc.fib.perez.lluis.cluki.entities.CourseGenericInfo;

public class CourseSelection extends AppCompatActivityCluki {

    private static final int REQUEST_IMAGE_CAPTURE = 2;
    private static final int RESULT_LOAD_IMG = 3;
    private static final int RESULT_CODE = 1;
    private String text;

    private MenuItem bin;
    private int position;
    private CourseGenericInfo[] courseGenericInfoList;
    private GridViewCourseAdapter baseAdapter;

    private CourseDAO courseDAO;
    private RelationDAO relationDAO;
    private ProcessDAO processDAO;
    private KidDAO kidDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course_selection);
        courseDAO = new CourseDAO(this);
        relationDAO = new RelationDAO(this);
        processDAO = new ProcessDAO(this);
        kidDAO = new KidDAO(this);
        initContext();
        initToolbar("Mis Cursos", R.id.base_toolbar);
        initGridView();
        floatingButtonListener();
    }

    private void initGridView() {
        courseGenericInfoList = (new CourseGenericInfoDAO(this)).getCourseGenericInfo();
        GridView gridView = (GridView) findViewById(R.id.grid_view_course_selection);
        baseAdapter = new GridViewCourseAdapter(this, courseGenericInfoList);
        gridView.setAdapter(baseAdapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                Intent shareIntent = new Intent(v.getContext(), KidInformationSelection.class);
                shareIntent.putExtra(getString(R.string.course), courseGenericInfoList[position].getCourseId());
                shareIntent.putExtra("name", courseGenericInfoList[position].getCourseName());
                startActivityForResult(shareIntent, RESULT_CODE);
            }
        });
        gridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                           int pos, long arg3) {
                bin.setVisible(true);
                position = pos;
                return true;
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_FINISH) {
            finish();
        }
    }

    private void createNewCourse(String name) {
        Integer id = courseDAO.getNewCourseId();
        createNewCourse(id, name);
        Intent i = new Intent(this, CourseCreation.class);
        i.putExtra("id", id);
        i.putExtra("name", name);
        startActivityForResult(i, 1);
        initGridView();
    }

    private void createNewCourse(Integer id, String name) {
        Course course = new Course();
        course.setName(text);
        course.setId(id);
        courseDAO.insertCourse(course);
    }


    private void floatingButtonListener() {
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fabClass);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                createDialog();
            }
        });
    }

    private void createDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setView(R.layout.dialog_inputtext)
                .setTitle("¿Que nombre quieres ponerle?")
                .setPositiveButton("Crear", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        EditText editText = (EditText) ((AlertDialog) dialog).findViewById(R.id.dialog_edit_text);
                        if (editText != null && !String.valueOf(editText.getText()).isEmpty()) {
                            text = String.valueOf(editText.getText());
                            createNewCourse(text);

                        }
                    }
                })
                .show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_edit, menu);
        menu.findItem(R.id.edit).setVisible(false);
        bin = menu.findItem(R.id.erase);
        bin.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.erase:
                deleteClassFromGridView();
                break;
        }
        bin.setVisible(false);
        return true;
    }

    private void deleteClassFromGridView() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("¿Estas seguro que quieres eliminarlo?")
                .setMessage("Se borrarán todos los procesos y los niños del curso escogido")
                .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        deleteAction();
                    }
                })
                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                })
                .show();
    }

    private void deleteAction() {
        int id = courseGenericInfoList[position].getCourseId();
        deleteAllKidImageFromCourse(id);
        deleteAllProccesImageFromCourse(id);
        kidDAO.deleteKidsByCourse(id);
        processDAO.deleteProcessByCourse(id);
        courseDAO.deleteCourse(id);
        baseAdapter.remove(position);
        baseAdapter.notifyDataSetChanged();
        Toast.makeText(mContext, "El curso ha sido eliminado", Toast.LENGTH_SHORT).show();
    }

    private void deleteAllProccesImageFromCourse(int id) {
        Integer[] processes = processDAO.getProcessesIdByCourse(id);
        for (Integer process : processes) {
            crypto.deleteFile("process_" + process);
            relationDAO.deleteRelationByProcess(process);
        }
    }

    private void deleteAllKidImageFromCourse(int id) {
        Kid[] kids = kidDAO.getKidsByCourse(id);
        for (Kid kid : kids) {
            crypto.deleteFile("kid_" + kid.getId());
        }
    }

}
