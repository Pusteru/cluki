package upc.fib.perez.lluis.cluki.dao;

import android.content.Context;
import android.database.Cursor;
import upc.fib.perez.lluis.cluki.dto.Kid;
import upc.fib.perez.lluis.cluki.entities.KidNumber;
import upc.fib.perez.lluis.cluki.entities.KidNumbers;

public class KidNumbersDAO extends BaseDAO{

    public KidNumbersDAO(Context context) {
        super(context);
    }

    public KidNumbers getKidsExtrovert(int process) {
        String query = "select k.id,k.name,k.last_name,count(*) as number from relations r inner join kids " +
                "k on r.subject_id = k.id and r.user = k.user where process_id = ? and k.user = ? and friend_id <> -1 group by k.id,k.name order by number desc";
        return getKidNumberFrom(query, process);
    }

    public KidNumbers getKidsFriends(int process) {
        String query = "select k.id,k.name,k.last_name,count(*) as number from relations r inner join kids " +
                "k on r.friend_id = k.id and k.user = r.user where process_id = ? and k.user = ? and friend_id <> -1 group by k.id,k.name order by number desc";
        return getKidNumberFrom(query, process);
    }

    private KidNumbers getKidNumberFrom(String query, int process) {
        KidNumbers kidNumbers = new KidNumbers();
        Cursor c = db.rawQuery(query, new String[]{String.valueOf(process), this.getUser()});
        while (c.moveToNext()) {
            String name = c.getString(c.getColumnIndex(Kid.getColumnName()));
            int number = c.getInt(c.getColumnIndex("number"));
            kidNumbers.addKid(new KidNumber(name, number));
        }
        c.close();
        return kidNumbers;
    }


}
