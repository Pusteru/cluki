package upc.fib.perez.lluis.cluki.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;
import com.facebook.stetho.Stetho;
import com.facebook.stetho.okhttp3.StethoInterceptor;
import okhttp3.OkHttpClient;
import upc.fib.perez.lluis.cluki.R;
import upc.fib.perez.lluis.cluki.db.DBHelper;
import upc.fib.perez.lluis.cluki.session.UserSessionManager;
import upc.fib.perez.lluis.cluki.utils.CryptoUtils;

/**
 * Created by Pusteru on 02/07/2017.
 */
public class AppCompatActivityCluki extends AppCompatActivity {

    CryptoUtils crypto;
    static final int RESULT_FINISH = 9;
    static final int REQUEST_BASIC = 0;
    UserSessionManager userSessionManager;

    Context mContext;

    void initToolbar(String name, int menu) {
        Toolbar toolbar = (Toolbar) findViewById(menu); // Attaching the layout to the toolbar object
        setSupportActionBar(toolbar);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setTitle(null);
        }
        TextView textView = (TextView) findViewById(R.id.kidNameSelection);
        textView.setText(name);
        mContext = this;
    }

    void initContext(){
        crypto = new CryptoUtils(getApplicationContext());

        userSessionManager = new UserSessionManager(this);

        Stetho.initializeWithDefaults(this);
        new OkHttpClient.Builder()
                .addNetworkInterceptor(new StethoInterceptor())
                .build();
    }


    public void onLogoPress(View v){
        Intent returnIntent = new Intent();
        setResult(RESULT_FINISH, returnIntent);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    public Integer getInteger(int resource){
        return getResources().getInteger(resource);
    }

}
