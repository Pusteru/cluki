package upc.fib.perez.lluis.cluki.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import org.apache.commons.lang3.ArrayUtils;
import upc.fib.perez.lluis.cluki.R;
import upc.fib.perez.lluis.cluki.dto.User;

public class GridViewProfessorAdapter extends BaseAdapter {

    private Context context;
    private User[] users;

    public GridViewProfessorAdapter(Activity context, User[] users) {
        this.context = context;
        this.users = users;
    }

    @Override
    public int getCount() {
        return users.length;
    }

    @Override
    public Object getItem(int position) {
        return users[position];
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextHolder holder;
        if (convertView == null) {
            holder = new TextHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.gridview_item_professor, parent, false);
            holder.text = (TextView) convertView.findViewById(R.id.id);
            holder.text2 = (TextView) convertView.findViewById(R.id.password);
            convertView.setTag(holder);
        } else {
            holder = (TextHolder) convertView.getTag();
        }
        holder.text.setText(users[position].getId());
        holder.text2.setText(users[position].getPassword());
        return convertView;
    }

    public void addUser(User user) {
        users = ArrayUtils.add(users, user);
    }

    public void removeUser(int position) {
        users = ArrayUtils.remove(users, position);
    }

    public void modifyUser(int position, User user) {
        users[position] = user;
    }
}
