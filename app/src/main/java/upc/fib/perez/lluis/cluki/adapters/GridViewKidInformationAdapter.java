package upc.fib.perez.lluis.cluki.adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import org.apache.commons.lang3.ArrayUtils;
import upc.fib.perez.lluis.cluki.R;
import upc.fib.perez.lluis.cluki.entities.KidInformation;
import upc.fib.perez.lluis.cluki.utils.CryptoUtils;

import java.io.ByteArrayOutputStream;

public class GridViewKidInformationAdapter extends BaseAdapter {

    private final CryptoUtils crypto;
    private final Context context;
    private KidInformation[] kids;
    private boolean imageView;

    public GridViewKidInformationAdapter(Context context, KidInformation[] kids, CryptoUtils crypto) {
        this.context = context;
        this.kids = kids;
        this.crypto = crypto;
        SharedPreferences sharedPref = context.getSharedPreferences("Preferences", Context.MODE_PRIVATE);
        String user = sharedPref.getString("user", "admin");
        imageView = sharedPref.getString(user + "_image", "true").equals("true");
    }

    public void remove(int position) {
        kids = ArrayUtils.remove(kids, position);
    }


    @Override
    public int getCount() {
        return kids.length;
    }

    @Override
    public Object getItem(int position) {
        return kids[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private Bitmap getImage(KidInformation selected) {
        String file = "kid_" + selected.getId();
        return this.crypto.decodeFile(file);
    }

    private byte[] bitmapToByte(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        return stream.toByteArray();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        InformationHolder holder;
        if (convertView == null) {
            holder = new InformationHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.gridview_item_kid_information, parent, false);
            holder.textView = (TextView) convertView.findViewById(R.id.title);
            holder.imageView = (ImageView) convertView.findViewById(R.id.thumbnail);
            holder.group = (ImageView) convertView.findViewById(R.id.group_color);
            holder.process = (TextView) convertView.findViewById(R.id.process_number);
            convertView.setTag(holder);
        } else {
            holder = (InformationHolder) convertView.getTag();
        }

        KidInformation kid = kids[position];

        if (imageView) {
            Glide.with(holder.imageView.getContext()).asBitmap().load(bitmapToByte(getImage(kid))).into(holder.imageView);
        }

        holder.textView.setText(kid.getName());
        holder.process.setText(String.valueOf(kid.getProcess()));
        Glide.with(holder.group.getContext()).load(context.getResources().getIdentifier("group_" + kid.getGroup(), "drawable", context.getPackageName())).into(holder.group);
        return convertView;
    }
}
