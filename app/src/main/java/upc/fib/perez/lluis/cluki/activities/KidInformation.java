package upc.fib.perez.lluis.cluki.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import upc.fib.perez.lluis.cluki.R;
import upc.fib.perez.lluis.cluki.dao.ProcessDAO;
import upc.fib.perez.lluis.cluki.dto.Process;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;

public class KidInformation extends AppCompatActivityCluki {
    private static final int DEFAULT_INTENT_VALUE = 0;
    private ListIterator<Process> iterator;
    private List<Process> processSet;
    private ProcessDAO processDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kid_information);
        processDAO = new ProcessDAO(this);
        initContext();
        initView();
    }

    private void initView() {
        Intent i = this.getIntent();
        Integer kidId = i.getIntExtra(getString(R.string.id), DEFAULT_INTENT_VALUE);
        String name = i.getStringExtra(getString(R.string.name));
        initToolbar(name, R.id.base_toolbar);
        if (canLoad()) {
            loadKidImage(kidId);
        }
        processSet = processDAO.getProcessFor(kidId);
        loadFirstElement();
    }

    private boolean canLoad() {
        SharedPreferences sharedPref = this.getSharedPreferences("Preferences", Context.MODE_PRIVATE);
        String user = sharedPref.getString("user", "admin");
        return sharedPref.getString(user + "_image", "true").equals("true");
    }

    private void loadKidImage(Integer kidId) {
        Bitmap kidPhoto = getKidPhoto(kidId);
        ImageView imageView = (ImageView) findViewById(R.id.kidPhoto);
        imageView.setImageBitmap(kidPhoto);
    }

    private void loadFirstElement() {
        iterator = processSet.listIterator();
        if (iterator.hasNext()) {
            Process p = iterator.next();
            load(p.getId(), p.getDate());
        }
    }

    public void nextProcess(View v) {
        if (iterator.hasNext()) {
            Process p = iterator.next();
            load(p.getId(), p.getDate());
        } else {
            loadFirstElement();
        }
    }

    private void getIteratorLastElement() {
        while (iterator.hasNext()) {
            iterator.next();
        }
    }

    public void previousProcess(View v) {
        if (!iterator.hasPrevious()) {
            getIteratorLastElement();
        }
        Process p = iterator.previous();
        load(p.getId(), p.getDate());
    }

    private void load(Integer id, Date date) {
        loadImage(id);
        loadDate(date);
    }

    private void loadImage(Integer id) {
        ImageView graph = (ImageView) findViewById(R.id.graphImage);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        Bitmap bitmap = BitmapFactory.decodeFile(this.getFilesDir() + "/process_" + id + ".png", options);
        graph.setImageBitmap(bitmap);
    }

    private void loadDate(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ITALIAN);
        TextView textView = (TextView) findViewById(R.id.textView3);
        textView.setText(sdf.format(date));
    }

    private Bitmap getKidPhoto(Integer kidId) {
        return this.crypto.decodeFile("kid_" + kidId);
    }


}
