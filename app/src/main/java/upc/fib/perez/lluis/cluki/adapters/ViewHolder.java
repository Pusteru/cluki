package upc.fib.perez.lluis.cluki.adapters;

import android.widget.ImageView;
import android.widget.TextView;

class ViewHolder {
    TextView textView;
    ImageView imageView;
}
