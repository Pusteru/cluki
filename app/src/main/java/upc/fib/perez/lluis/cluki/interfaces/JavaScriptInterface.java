package upc.fib.perez.lluis.cluki.interfaces;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.widget.Toast;
import upc.fib.perez.lluis.cluki.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

/**
 * Created by Pusteru on 03/09/2017.
 */
public class JavaScriptInterface {
    private final int processId;
    private Activity mContext;
    private String nodes;
    private String edges;
    private String kidsExtrovertRate;
    private String kidsFriendlyRate;

    /**
     * Instantiate the interface and set the context
     */
    public JavaScriptInterface(Activity c, String nodes, String edges, String kidsExtrovertRate, String kidsFriendlyRate, int processId) {
        mContext = c;
        this.nodes = nodes;
        this.edges = edges;
        this.kidsExtrovertRate = kidsExtrovertRate;
        this.kidsFriendlyRate = kidsFriendlyRate;
        this.processId = processId;
    }

    /**
     * Show a toast from the web page
     */
    @JavascriptInterface
    public void showToast(String toast) {
        Toast.makeText(mContext, toast, Toast.LENGTH_SHORT).show();
    }

    @JavascriptInterface
    public String getNodes() {
        return nodes;
    }

    public void setNodes(String nodes) {
        this.nodes = nodes;
    }

    @JavascriptInterface
    public String getEdges() {
        return edges;
    }

    public void setEdges(String edges) {
        this.edges = edges;
    }

    @JavascriptInterface
    public String getKidsExtrovertRate() {
        return kidsExtrovertRate;
    }

    public void setKidsExtrovertRate(String kidsExtrovertRate) {
        this.kidsExtrovertRate = kidsExtrovertRate;
    }

    @JavascriptInterface
    public String getKidsFriendlyRate() {
        return kidsFriendlyRate;
    }

    public void setKidsFriendlyRate(String kidsFriendlyRate) {
        this.kidsFriendlyRate = kidsFriendlyRate;
    }

    @JavascriptInterface
    public void saveImage() {
        WebView wv = (WebView) mContext.findViewById(R.id.wv);
        Bitmap bitmap = Bitmap.createBitmap(wv.getWidth(), wv.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        wv.draw(canvas);
        try {
            File file = new File(mContext.getFilesDir(), "/process_" + processId + ".png");
            OutputStream fOut = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 50, fOut);
            fOut.flush();
            fOut.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Toast.makeText(mContext, "Se ha guardado el proceso", Toast.LENGTH_SHORT).show();
    }

    public void setmContext(Activity mContext) {
        this.mContext = mContext;
    }
}
