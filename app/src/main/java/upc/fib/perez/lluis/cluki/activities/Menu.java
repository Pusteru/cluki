package upc.fib.perez.lluis.cluki.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import upc.fib.perez.lluis.cluki.R;
import upc.fib.perez.lluis.cluki.adapters.GridViewMenuAdapter;
import upc.fib.perez.lluis.cluki.entities.MenuItem;
import upc.fib.perez.lluis.cluki.entities.MenuItems;
import upc.fib.perez.lluis.cluki.session.UserSessionManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pusteru on 26/08/2017.
 */
public class Menu extends AppCompatActivityCluki {

    private final MenuItems menuItems = new MenuItems();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        userSessionManager = new UserSessionManager(this);
        addMenuItems();
        initToolbar(userSessionManager.getUserName(), R.id.logout_toolbar);
        initGridView();
    }

    private void addMenuItems() {
        if (userSessionManager.isSuperUser()) {
            menuItems.addMenuItem(ProfessorCreation.class,getString(R.string.usersMenuTitle),R.drawable.professor_icon);
        } else {
            menuItems.addMenuItem(ProcessSelection.class,getString(R.string.sociogramMenuTitle),R.drawable.network);
            menuItems.addMenuItem(CourseSelection.class,getString(R.string.coursesMenuTitle),R.drawable.desk_chair);
            menuItems.addMenuItem(Settings.class,getString(R.string.settingsMenuTitle),R.drawable.settings);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        getMenuInflater().inflate(R.menu.menu_logout, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(android.view.MenuItem item) {
        if(item.getItemId() == R.id.logout){
            showLogOutWarning();
        }
        return true;
    }

    private void initGridView() {
        GridView gridView = (GridView) findViewById(R.id.grid_view_menu);
        gridView.setAdapter(new GridViewMenuAdapter(this, this.menuItems.toArray()));
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent,
                                    View v, int position, long id) {
                startActivity(new Intent(mContext, menuItems.getMenuItemClass(position)));
            }
        });
    }

    @Override
    public void onBackPressed() {
        showLogOutWarning();
    }

    @Override
    public void onLogoPress(View v) {
        showLogOutWarning();
    }

    private void showLogOutWarning() {
        AlertDialog.Builder builder = new
                AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(R.string.logOutWarningTitle);
        builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}


