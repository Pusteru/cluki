package upc.fib.perez.lluis.cluki.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import upc.fib.perez.lluis.cluki.R;
import upc.fib.perez.lluis.cluki.adapters.GridViewProfessorKidAdapter;
import upc.fib.perez.lluis.cluki.dao.KidDAO;
import upc.fib.perez.lluis.cluki.dto.Kid;
import upc.fib.perez.lluis.cluki.entities.ParticipationKid;
import upc.fib.perez.lluis.cluki.gridview.KidsGridViewWithText;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class KidProfessorSelection extends AppCompatActivityCluki {

    private static final int RESULT_CODE = 1;
    private static final int DEFAULT_INTENT_VALUE = 0;
    private static final int RESULT_FINISH = 9;
    private View lastView;
    private Integer process;
    private KidDAO kidDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kid_professor_selection);
        kidDAO = new KidDAO(this);

        initContext();
        initGridView();
        initToolbar("Seleccione a un niño", R.id.base_toolbar);
        floatingButtonListener();
    }


    private void floatingButtonListener() {
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                endProcess();
            }
        });
    }

    private void initGridView() {
        GridView gridView = (GridView) findViewById(R.id.grid_view_preview);
        final ParticipationKid[] list = this.getKidListFromIntent();
        BaseAdapter baseAdapter = new GridViewProfessorKidAdapter(this, this.crypto, list);
        new KidsGridViewWithText((GridView) findViewById(R.id.grid_view_preview), baseAdapter, list);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent,
                                    View v, int position, long id) {
                lastView = v;
                Intent shareIntent = new Intent(v.getContext(), KidSelection.class);
                shareIntent.putExtra(getString(R.string.course), list[0].getKid().getCourseId());
                shareIntent.putExtra(getString(R.string.position), position);
                shareIntent.putExtra(getString(R.string.name), list[position].getKid().getName());
                shareIntent.putExtra(getString(R.string.id), list[position].getKid().getId());
                shareIntent.putExtra(getString(R.string.process), process);
                startActivityForResult(shareIntent, RESULT_CODE);
            }
        });
    }

    private ParticipationKid[] getKidListFromIntent() {
        Intent i = this.getIntent();
        process = i.getIntExtra(getString(R.string.process), DEFAULT_INTENT_VALUE);
        Integer course = i.getIntExtra(getString(R.string.course), DEFAULT_INTENT_VALUE);
        Kid[] kids = kidDAO.getKidsByCourse(course);
        Set<Integer> participants = kidDAO.getParticipationKidsIn(process);
        return getParticipationKids(kids, participants);
    }

    private ParticipationKid[] getParticipationKids(Kid[] kids, Set<Integer> participants) {
        List<ParticipationKid> ret = new ArrayList<>();
        for (Kid kid : kids) {
            ret.add(new ParticipationKid(kid, participants.contains(kid.getId())));
        }
        ParticipationKid[] participationKids = new ParticipationKid[ret.size()];
        return ret.toArray(participationKids);
    }

    private void endProcess() {
        Intent shareIntent = new Intent(this, GraphView.class);
        shareIntent.putExtra(getString(R.string.process), process);
        startActivityForResult(shareIntent, RESULT_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            lastView.findViewById(R.id.thumbnail).setBackgroundColor(Color.GREEN);
        } else if (resultCode == RESULT_FINISH) {
            finish();
        } else {
            lastView.findViewById(R.id.thumbnail).setBackgroundColor(Color.BLACK);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }


}
