package upc.fib.perez.lluis.cluki.dao;

import android.content.Context;
import android.database.Cursor;
import upc.fib.perez.lluis.cluki.entities.KidInformation;

import java.util.ArrayList;
import java.util.List;

public class KidInformationDAO extends BaseDAO{

    private static final String SELECT_KID_INFORMATION = "select name, id, sum(popular)*1.0/sum(participation) as rate," +
            " count(distinct process_id) as process from (select k.name,k.id, process_id, participation,count(*) as popular " +
            "from kids k left join relations r on k.id = r.subject_id and k.user = r.user " +
            "left join (select p.id, count(distinct r.subject_id) as participation from relations r" +
            " inner join process p on p.id = r.process_id and p.user = r.user where p.course_id = ? and p.user = ? " +
            "group by p.id) s on process_id = s.id where k.course_id = ? and k.user = ?" +
            " group by k.name, k.id, process_id, participation) group by name, id;";

    public KidInformationDAO(Context context) {
        super(context);
    }

    public KidInformation[] getKidInformationBy(Integer courseId) {
        List<KidInformation> ret = new ArrayList<>();
        String user = this.getUser();
        String selectionArgs[] = new String[]{String.valueOf(courseId), user, String.valueOf(courseId), user};

        Cursor c = db.rawQuery(SELECT_KID_INFORMATION, selectionArgs);
        while (c.moveToNext()) {
            KidInformation kid = new KidInformation(c);

            ret.add(kid);
        }
        c.close();
        return ret.toArray(new KidInformation[ret.size()]);
    }

}
