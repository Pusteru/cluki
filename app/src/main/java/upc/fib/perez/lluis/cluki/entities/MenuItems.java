package upc.fib.perez.lluis.cluki.entities;

import java.util.ArrayList;
import java.util.List;

public class MenuItems {
    private List<MenuItem> menuItems;

    public MenuItems() {
        this.menuItems = new ArrayList<>();
    }

    public void addMenuItem(Class action, String title, int drawingId){
        menuItems.add(new MenuItem(title,drawingId,action));

    }

    public MenuItem[] toArray() {
        return menuItems.toArray(new MenuItem[menuItems.size()]);
    }

    public Class getMenuItemClass(int position) {
        return menuItems.get(position).getAction();
    }
}
