package upc.fib.perez.lluis.cluki.session;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import upc.fib.perez.lluis.cluki.activities.Login;

import java.util.HashMap;

public class UserSessionManager {

    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private Context context;

    private static final String PREFER_NAME = "Preferences";
    private static final String IS_USER_LOGIN = "IsUserLoggedIn";
    public static final String IS_SUPER_USER = "IsSuperUser";
    public static final String KEY_NAME = "user";

    public UserSessionManager(Context context){
        this.context = context;
        int PRIVATE_MODE = 0;
        pref = this.context.getSharedPreferences(PREFER_NAME, PRIVATE_MODE);
        editor = pref.edit();
        editor.apply();
    }

    public void createUserLoginSession(String name, boolean superuser){
        editor.putString(KEY_NAME, name);
        editor.putBoolean(IS_SUPER_USER, superuser);
        editor.commit();
    }

    public void logoutUser(){
        editor.clear();
        editor.commit();
        Intent i = new Intent(context, Login.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i);
    }

    public String getUserName() {
        return pref.getString(KEY_NAME,"admin");
    }

    public boolean isSuperUser() {
        return pref.getBoolean(IS_SUPER_USER,false);
    }
}
