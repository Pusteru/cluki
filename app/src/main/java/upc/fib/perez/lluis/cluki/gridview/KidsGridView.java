package upc.fib.perez.lluis.cluki.gridview;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import upc.fib.perez.lluis.cluki.R;
import upc.fib.perez.lluis.cluki.entities.ParticipationKid;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Pusteru on 29/04/2017.
 */

public class KidsGridView {

    private final GridView gridView;

    private final ParticipationKid[] kids;

    private final Set<Integer> selected;


    public KidsGridView(GridView gridView, BaseAdapter imageAdapter, ParticipationKid[] list) {
        this.gridView = gridView;
        this.gridView.setAdapter(imageAdapter);
        this.kids = list;
        selected = populateSelected();
    }

    private Set<Integer> populateSelected() {
        Set<Integer> selected = new HashSet<>();
        for (ParticipationKid kid : kids) {
            if (kid.isParticipation()) {
                selected.add(kid.getKid().getId());
            }
        }
        return selected;
    }

    public void selectionOfKid() {
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent,
                                    View v, int position, long id) {
                View view = v.findViewById(R.id.thumbnail);
                int color = ((ColorDrawable) view.getBackground()).getColor();
                if (color == Color.BLACK) {
                    color = Color.GREEN;
                    selected.add(kids[position].getKid().getId());
                } else if (color == Color.GREEN) {
                    color = Color.BLACK;
                    selected.remove(kids[position].getKid().getId());
                }
                view.setBackgroundColor(color);
            }
        });
    }

    public ArrayList<Integer> getSelectedKids() {
        return new ArrayList<>(selected);
    }


}
