package upc.fib.perez.lluis.cluki.entities;

/**
 * Created by Pusteru on 08/09/2017.
 */
public class KidNumber {
    private String name;
    private int number;

    public KidNumber(String name, int number) {
        this.name = name;
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
}
