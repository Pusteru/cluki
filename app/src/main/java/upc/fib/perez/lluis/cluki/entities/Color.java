package upc.fib.perez.lluis.cluki.entities;

public class Color {
    private String background;
    private String border;

    public String getBackground() {
        return background;
    }

    public void setBackground(String background) {
        this.background = background;
    }

    public String getBorder() {
        return border;
    }

    public void setBorder(String border) {
        this.border = border;
    }
}
