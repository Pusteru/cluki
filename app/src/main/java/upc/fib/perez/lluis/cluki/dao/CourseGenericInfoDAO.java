package upc.fib.perez.lluis.cluki.dao;

import android.content.Context;
import android.database.Cursor;
import upc.fib.perez.lluis.cluki.entities.CourseGenericInfo;

import java.util.ArrayList;
import java.util.List;

public class CourseGenericInfoDAO extends BaseDAO{

    public CourseGenericInfoDAO(Context context) {
        super(context);
    }

    public CourseGenericInfo[] getCourseGenericInfo() {
        List<CourseGenericInfo> courseGenericInfoList = new ArrayList<>();
        Cursor c = db.rawQuery("select c.id as course_id,c.name as course_name, count(distinct p.id) " +
                "as process_number, count (distinct k.id) as kid_number from courses c left join kids k on " +
                "c.id = k.course_id and k.user  = c.user left join process p on c.id = p.course_id and c.user = p.user where c.user = ? group by c.id", new String[]{this.getUser()});
        while (c.moveToNext()) {
            courseGenericInfoList.add(new CourseGenericInfo(c));
        }
        c.close();
        return courseGenericInfoList.toArray(new CourseGenericInfo[courseGenericInfoList.size()]);
    }

}
