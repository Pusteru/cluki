package upc.fib.perez.lluis.cluki.dto;

import android.database.Cursor; /**
 * Created by Pusteru on 22/04/2017.
 */

public class Kid {

    private Integer id;
    private String name;
    private String lastName;
    private Integer courseId;

    private static final String tableName = "kids";
    private static final String columnId = "id";
    private static final String columnName = "name";
    private static final String columnLastName = "last_name";
    private static final String columnCourseId = "course_id";
    private static final String columnUser = "user";

    public Kid(Cursor c) {
        setId(c.getInt(c.getColumnIndex(Kid.getColumnId())));
        setCourseId(c.getInt(c.getColumnIndex(Kid.getColumnCourseId())));
        setName(c.getString(c.getColumnIndex(Kid.getColumnName())));
        setLastName(c.getString(c.getColumnIndex(Kid.getColumnLastName())));
    }

    public Kid() {

    }


    public static String getTableName() {
        return tableName;
    }

    public static String getColumnId() {
        return columnId;
    }

    public static String getColumnName() {
        return columnName;
    }

    public static String getColumnLastName() {
        return columnLastName;
    }

    public static String getColumnCourseId() {
        return columnCourseId;
    }

    public static String getColumnUser() {
        return columnUser;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getCourseId() {
        return courseId;
    }

    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }


}
