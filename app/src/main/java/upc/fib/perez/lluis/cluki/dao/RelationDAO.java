package upc.fib.perez.lluis.cluki.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import upc.fib.perez.lluis.cluki.dto.Kid;
import upc.fib.perez.lluis.cluki.dto.Process;
import upc.fib.perez.lluis.cluki.dto.Relation;
import upc.fib.perez.lluis.cluki.dto.User;
import upc.fib.perez.lluis.cluki.entities.Edge;
import upc.fib.perez.lluis.cluki.entities.Edges;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class RelationDAO extends BaseDAO{

    private static final String createRelationsTable = String.format("create table %s (%s integer, %s integer, " +
                    "%s integer, %s text,foreign key (%s) references %s (%s),foreign key (%s) references %s (%s), foreign key (%s) references %s (%s), " +
                    "foreign key (%s) references %s (%s)) ", Relation.getTableName(), Relation.getColumnProcessId(), Relation.getColumnSubjectId(),
            Relation.getColumnFriendId(), Relation.getColumnUser(), Relation.getColumnUser(), User.getTableName(), User.getColumnId(),
            Relation.getColumnProcessId(), Process.getTableName(), Process.getColumnId(),
            Relation.getColumnSubjectId(), Kid.getTableName(), Kid.getColumnId(),
            Relation.getColumnFriendId(), Kid.getTableName(), Kid.getColumnId());

    public RelationDAO(Context context) {
        super(context);
    }

    public static String getCreateRelationsTable() {
        return createRelationsTable;
    }

    public void cleanRelationsOf(Integer processId, Integer subjectKid){
        db.execSQL("DELETE FROM " + Relation.getTableName() + " WHERE " + Relation.getColumnProcessId() +
                " = " + processId + " and " + Relation.getColumnSubjectId() + " = " + subjectKid);
    }

    public void insertRelations(Integer processId, Integer subjectKid, ArrayList<Integer> relations) {
        ContentValues values = new ContentValues();
        if (relations.isEmpty()) {
            relations.add(-1);
        }

        for (Integer friend : relations) {
            values.put(Relation.getColumnProcessId(), processId);
            values.put(Relation.getColumnSubjectId(), subjectKid);
            values.put(Relation.getColumnFriendId(), friend);
            values.put(Relation.getColumnUser(), this.getUser());
            db.insert(Relation.getTableName(), null, values);
        }
    }

    public Set<Integer> getKidFriendsIn(Integer process, Integer kid) {
        String query = "select friend_id from relations where subject_id = ? and process_id = ? and user = ?";

        Set<Integer> ret = new HashSet<>();
        String selectionArgs[] = new String[]{String.valueOf(kid),String.valueOf(process),this.getUser()};
        Cursor c = db.rawQuery(query,selectionArgs);

        while(c.moveToNext()){
            ret.add(c.getInt(c.getColumnIndex(Relation.getColumnFriendId())));
        }
        c.close();
        return ret;
    }

    public void deleteRelationByProcess(Integer process) {
        db.delete(Relation.getTableName(), "process_id = " + process, null);
    }

    public void deleteRelationByKid(Integer id) {
        db.delete(Relation.getTableName(), "subject_id = " + id, null);
        db.delete(Relation.getTableName(), "friend_id = " + id, null);
    }

    public Edges getEdges(int process) {
        Edges edges = new Edges();
        String query = "select subject_id, friend_id from relations where process_id = ? and user = ? and friend_id <> -1";
        Cursor c = db.rawQuery(query, new String[]{String.valueOf(process), this.getUser()});
        while (c.moveToNext()) {
            int from = c.getInt(c.getColumnIndex(Relation.getColumnSubjectId()));
            int to = c.getInt(c.getColumnIndex(Relation.getColumnFriendId()));
            edges.addEdge(new Edge(from, to));
        }
        c.close();
        return edges;
    }

}
