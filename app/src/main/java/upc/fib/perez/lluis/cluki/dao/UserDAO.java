package upc.fib.perez.lluis.cluki.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import upc.fib.perez.lluis.cluki.dto.Kid;
import upc.fib.perez.lluis.cluki.dto.User;
import upc.fib.perez.lluis.cluki.enums.RoleEnum;

import java.util.ArrayList;
import java.util.List;

public class UserDAO extends BaseDAO{

    public UserDAO(Context context){
        super(context);
    }

    private static final String CREATE_USERS_TABLE = String.format("create table %s (%s text primary key, " +
            "%s string not null, %s integer not null)", User.getTableName(), User.getColumnId(), User.getColumnPassword(), User.getColumnRole());

    public static String getCreateUsersTable() {
        return CREATE_USERS_TABLE;
    }

    public User[] getUsers() {
        List<User> users = new ArrayList<>();
        try(Cursor c = db.rawQuery("select * from users", new String[0])) {
            while (c.moveToNext()) {
                users.add(new User(c));
            }
        }
        return users.toArray(new User[users.size()]);
    }

    public void removeUser(String id) {
        db.delete(User.getTableName(), "id = '" + id + "'", null);
    }

    public void updateUser(String id, User user) {
        ContentValues cv = new ContentValues();
        cv.put(User.getColumnId(), user.getId());
        cv.put(User.getColumnPassword(), user.getPassword());
        db.update(Kid.getTableName(), cv, "id = '" + id + "'", null);
    }

    public void insertUser(User user) {
        ContentValues values = new ContentValues();
        values.put(User.getColumnId(), user.getId());
        values.put(User.getColumnPassword(), user.getPassword());
        values.put(User.getColumnRole(), user.getRole().getValue());
        db.insert(User.getTableName(), null, values);
    }

    public RoleEnum getRole(String mEmail, String mPassword) {
        RoleEnum role = RoleEnum.UNREGISTERED;
        Cursor c = db.rawQuery("select role from users where id = ? and password = ?",
                new String[]{mEmail, mPassword});
        if (c.moveToNext()) {
            role = RoleEnum.forCode(c.getInt(c.getColumnIndex("role")));
        }
        c.close();
        return role;
    }


}
