package upc.fib.perez.lluis.cluki.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import upc.fib.perez.lluis.cluki.dto.Course;
import upc.fib.perez.lluis.cluki.dto.Kid;
import upc.fib.perez.lluis.cluki.dto.Relation;
import upc.fib.perez.lluis.cluki.dto.User;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class KidDAO extends BaseDAO{

    public KidDAO(Context context){
        super(context);
    }

    private static final String createKidsTable = String.format("create table %s (%s integer primary" +
                    " key, %s text, %s text, %s integer, %s text,foreign key (%s) references %s (%s), foreign key (%s) references %s (%s))",
            Kid.getTableName(), Kid.getColumnId(), Kid.getColumnName(), Kid.getColumnLastName(),
            Kid.getColumnCourseId(), Kid.getColumnUser(), Kid.getColumnUser(), User.getTableName(), User.getColumnId(),
            Kid.getColumnCourseId(), Course.getTableName(), Course.getColumnId());

    public static String getCreateKidsTable() {
        return createKidsTable;
    }

    public Set<Integer> getFriendsOf(Integer process, Integer kid) {
        String query = "select friend_id from relations where subject_id = ? and process_id = ? and user = ?";
        Set<Integer> ret = new HashSet<>();
        Cursor c = db.rawQuery(query, new String[]{String.valueOf(kid), String.valueOf(process), this.getUser()});

        while (c.moveToNext()) {
            ret.add(c.getInt(c.getColumnIndex(Relation.getColumnFriendId())));
        }
        c.close();
        return ret;
    }

    public Kid[] getKidsByCourse(Integer courseId) {
        List<Kid> ret = new ArrayList<>();

        String selectionArgs[] = new String[]{String.valueOf(courseId), this.getUser()};
        Cursor c = db.rawQuery(String.format("select * from %s where course_id = ? and user = ?", Kid.getTableName()), selectionArgs);
        while (c.moveToNext()) {
            ret.add(new Kid(c));
        }
        c.close();
        return ret.toArray(new Kid[ret.size()]);
    }

    public Integer getNewKidId() {
        return getMaxIdFrom(Kid.getTableName(), Kid.getColumnId());
    }

    public void insertKid(Kid kid) {
        ContentValues values = new ContentValues();
        values.put(Kid.getColumnId(), kid.getId());
        values.put(Kid.getColumnName(), kid.getName());
        values.put(Kid.getColumnLastName(), kid.getLastName());
        values.put(Kid.getColumnCourseId(), kid.getCourseId());
        values.put(Kid.getColumnUser(), this.getUser());

        db.insert(Kid.getTableName(), null, values);
    }

    public void updateKid(Kid kid) {
        ContentValues cv = new ContentValues();
        cv.put(Kid.getColumnName(), kid.getName());
        db.update(Kid.getTableName(), cv, "id =" + kid.getId(), null);
    }

    public void deleteKid(Integer id) {
        db.delete(Kid.getTableName(), "id = " + id, null);
    }


    public void deleteKidsByCourse(int id) {
        db.delete(Kid.getTableName(), "course_id = " + id, null);
    }

    public Set<Integer> getParticipationKidsIn(Integer process) {
        String query = String.format("select k.%s from %s k inner join %s r on r.%s = k.%s where %s = ? and k.user = ? group by k.%s",
                Kid.getColumnId(), Kid.getTableName(), Relation.getTableName(), Relation.getColumnSubjectId(), Kid.getColumnId(), Relation.getColumnProcessId(), Kid.getColumnId());

        Set<Integer> ret = new HashSet<>();
        String selectionArgs[] = new String[]{String.valueOf(process), this.getUser()};
        Cursor c = db.rawQuery(query, selectionArgs);

        while (c.moveToNext()) {
            ret.add(c.getInt(c.getColumnIndex(Kid.getColumnId())));
        }
        c.close();
        return ret;
    }

}
