package upc.fib.perez.lluis.cluki.entities;

import java.util.Date;

public class ProcessParticipation {

    private Integer processId;
    private Integer courseKids;
    private Date processStartDate;
    private Integer participationKids;
    private Integer courseId;
    private String courseName;

    public Integer getProcessId() {
        return processId;
    }

    public void setProcessId(Integer processId) {
        this.processId = processId;
    }

    public Date getProcessStartDate() {
        return processStartDate;
    }

    public void setProcessStartDate(Date processStartDate) {
        this.processStartDate = processStartDate;
    }

    public Integer getCourseKids() {
        return courseKids;
    }

    public void setCourseKids(Integer courseKids) {
        this.courseKids = courseKids;
    }

    public Integer getParticipationKids() {
        return participationKids;
    }

    public void setParticipationKids(Integer participationKids) {
        this.participationKids = participationKids;
    }


    public Integer getCourseId() {
        return courseId;
    }

    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }
}
