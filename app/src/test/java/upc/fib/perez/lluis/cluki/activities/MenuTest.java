package upc.fib.perez.lluis.cluki.activities;

import android.app.AlertDialog;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowAlertDialog;
import upc.fib.perez.lluis.cluki.BuildConfig;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class MenuTest {

    @Test
    public void shouldAppearAlertOnBackPress(){
        //Robo doesn't support dialog v7 yet
    }



}